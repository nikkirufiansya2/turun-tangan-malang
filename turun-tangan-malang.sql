-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 03, 2021 at 01:47 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `turun-tangan-malang`
--

-- --------------------------------------------------------

--
-- Table structure for table `dokumentasi`
--

CREATE TABLE `dokumentasi` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dokumentasi`
--

INSERT INTO `dokumentasi` (`id`, `id_kegiatan`, `foto`) VALUES
(6, 5, 'Screenshot_from_2021-05-21_18-57-52.png'),
(7, 2, 'celluloid-shot00011.jpg'),
(8, 5, 'Screenshot_from_2021-06-19_09-03-52.png'),
(9, 1, 'Screenshot_from_2021-06-14_17-52-24.png'),
(10, 6, 'JokesApps21.png');

-- --------------------------------------------------------

--
-- Table structure for table `donatur`
--

CREATE TABLE `donatur` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `foto` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donatur`
--

INSERT INTO `donatur` (`id`, `id_users`, `email`, `nama`, `alamat`, `no_hp`, `foto`) VALUES
(2, 7, 'nikki@mail.com', 'nikki rufiansya pendonatur', 'watu gong', '081231413', 'CgjEgYWUUAEULI9.jpg'),
(4, 9, 'taufiq@mail.com', 'taufiq', 'watu gong', '0812312441', 'download1.jpeg'),
(5, 10, 'donatur2@mail.com', 'sang pendonatur', 'jl bla bla', '081233124', 'download.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `donatur_tiap_kegiatan`
--

CREATE TABLE `donatur_tiap_kegiatan` (
  `id` int(11) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donatur_tiap_kegiatan`
--

INSERT INTO `donatur_tiap_kegiatan` (`id`, `id_donatur`, `id_kegiatan`, `nama`, `status`) VALUES
(8, 2, 1, 'nikki rufiansya pendonatur', 'bergabung');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id` int(11) NOT NULL,
  `judul` varchar(250) NOT NULL,
  `status_kegiatan` varchar(250) NOT NULL,
  `pesan_ajakan` text NOT NULL,
  `deksripsi` text NOT NULL,
  `minimal_relawan` int(11) NOT NULL,
  `minimal_donasi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `foto_kegiatan` varchar(250) NOT NULL,
  `laporan_dana` text NOT NULL,
  `alamat` text NOT NULL,
  `file_laporan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `judul`, `status_kegiatan`, `pesan_ajakan`, `deksripsi`, `minimal_relawan`, `minimal_donasi`, `tanggal`, `foto_kegiatan`, `laporan_dana`, `alamat`, `file_laporan`) VALUES
(1, 'satu', 'Status Kegiatan 1', 'oke', 'oke', 10, 0, '2021-06-25', 'carbon.png', '1000000', 'jl candi 3 e malang jawa timur\r\n', 'laporan_keuangan1.xlsx'),
(2, 'dua', 'Status Kegiatan 1', 'aok', 'das', 2, 0, '2021-07-05', 'Screenshot_from_2021-06-19_09-03-522.png', '0', 'alun alun tugu malang', ''),
(5, 'tiga ', 'Status Kegiatan 2', 'ayoooo', 'oyaaaa', 23, 2000000, '2021-06-30', 'Screenshot_from_2021-06-19_09-03-521.png', '2000000', '', ''),
(6, 'empat', 'Status Kegiatan 1', 'ayo ikut', 'ok', 10, 15000000, '2021-06-21', 'celluloid-shot0001.jpg', '600000', 'batu malang jawa timur', 'laporan_keuangan.xlsx'),
(7, 'lima', 'Status Kegiatan 1', 'dasd', 'dasdasd', 10, 0, '2021-07-22', 'bukit_kelam.jpeg', '7000000', 'jl watu gong 31', 'user.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `isi_komentar` text NOT NULL,
  `waktu_komentar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id`, `id_kegiatan`, `id_donatur`, `isi_komentar`, `waktu_komentar`) VALUES
(5, 5, 5, 'Mantul', '0000-00-00'),
(6, 5, 4, 'taufiq komment', '0000-00-00'),
(7, 1, 4, 'tes koment 1', '0000-00-00'),
(8, 5, 2, 'nikki nyoba komentar', '0000-00-00'),
(9, 6, 5, 'mantap', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id`, `id_kegiatan`, `pesan`, `tanggal`) VALUES
(1, 2, 'ayo coy ngumpul jam 13', '2021-06-18'),
(2, 1, 'jgn lupa pakai baju kotor', '2021-06-18'),
(3, 6, 'ayo bsok ngumpul jam 13:00 di tempat', '2021-06-19');

-- --------------------------------------------------------

--
-- Table structure for table `relawan`
--

CREATE TABLE `relawan` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `divisi` varchar(250) NOT NULL,
  `profesi` varchar(250) NOT NULL,
  `fakultas` varchar(250) NOT NULL,
  `jurusan` varchar(250) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `no_hp` varchar(250) NOT NULL,
  `id_line` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `foto` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `relawan`
--

INSERT INTO `relawan` (`id`, `id_users`, `nama`, `divisi`, `profesi`, `fakultas`, `jurusan`, `alamat`, `no_hp`, `id_line`, `email`, `foto`) VALUES
(1, 2, 'relawan 2', 'PPG', 'Mahasiswa', 'Teknik', 'Teknik Elektro', 'Jl Watu GOng', '081231236721', 'realawan123123', 'relawan@mail.com', 'profile.jpg'),
(13, 14, 'nikki relawan', 'ppg', 'mahasiswa', 'teknik', 'IT', 'watu gong', '08123341233', 'Nikky142', 'nikki_relawan@mail.com', 'CgjEgYWUUAEULI91.jpg'),
(14, 15, 'dony_relawan', 'ppg', 'mahasiswa', 'Teknik', 'Teknik Mesin', 'Wtu gong', '08123213124', 'donny1234', 'donny_relawan@mail.com', 'logo-pkg10.png'),
(16, 17, 'Relawan A', 'KWU', 'Mahasiswa', 'Ekonomi', 'Ekonomi Syariah', 'Jl. Malang', '081234567889', 'relawana', 'relawan_a@mail.com', 'profile2.jpg'),
(17, 18, 'relawan b ', 'ppg', 'mahasiswa', 'teknik', 'teknik elektro', 'malang', '0812345657453', '8490234', 'relawan_b@mail.com', 'download.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `relawan_tiap_kegiatan`
--

CREATE TABLE `relawan_tiap_kegiatan` (
  `id` int(11) NOT NULL,
  `id_relawan` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `nama_relawan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `relawan_tiap_kegiatan`
--

INSERT INTO `relawan_tiap_kegiatan` (`id`, `id_relawan`, `id_kegiatan`, `nama_relawan`) VALUES
(1, 1, 1, 'relawan 2'),
(2, 13, 1, 'nikki relawan'),
(3, 1, 2, 'relawan 2'),
(4, 13, 2, 'nikki relawan'),
(5, 14, 2, 'dony_relawan'),
(6, 1, 5, 'relawan 2'),
(7, 13, 5, 'nikki relawan'),
(8, 1, 6, 'relawan 2'),
(9, 13, 6, 'nikki relawan');

-- --------------------------------------------------------

--
-- Table structure for table `status_kegiatan`
--

CREATE TABLE `status_kegiatan` (
  `id` int(11) NOT NULL,
  `status_kegiatan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status_kegiatan`
--

INSERT INTO `status_kegiatan` (`id`, `status_kegiatan`) VALUES
(1, 'Status Kegiatan 1'),
(2, 'Status Kegiatan 2');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_ambil_barang`
--

CREATE TABLE `transaksi_ambil_barang` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_ambil_barang`
--

INSERT INTO `transaksi_ambil_barang` (`id`, `id_kegiatan`, `id_donatur`, `nama_barang`, `alamat`) VALUES
(0, 7, 2, 'Buku', 'di perpustakan kota malang');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_barang`
--

CREATE TABLE `transaksi_barang` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `jasa_kirim` varchar(25) NOT NULL,
  `no_resi` varchar(90) NOT NULL,
  `status` varchar(10) NOT NULL,
  `waktu_donasi` date NOT NULL,
  `alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_barang`
--

INSERT INTO `transaksi_barang` (`id`, `id_kegiatan`, `id_donatur`, `deskripsi`, `jasa_kirim`, `no_resi`, `status`, `waktu_donasi`, `alamat`) VALUES
(1, 2, 2, 'siap', 'POS', '12312312', 'sucsess', '2021-06-18', 'das'),
(2, 6, 5, 'Buku pelajaran 1 pack', 'POS', '08321321312', 'sucsess', '2021-06-19', 'Jl Mlang');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_uang`
--

CREATE TABLE `transaksi_uang` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `bank` varchar(25) NOT NULL,
  `jumlah_donasi` int(11) NOT NULL,
  `waktu_donasi` date NOT NULL,
  `bukti_transfer` text NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_uang`
--

INSERT INTO `transaksi_uang` (`id`, `id_kegiatan`, `id_donatur`, `bank`, `jumlah_donasi`, `waktu_donasi`, `bukti_transfer`, `status`) VALUES
(1, 1, 2, 'BRI', 1000000, '2021-06-18', 'chatbot.jpeg', 'sucsess'),
(2, 2, 2, 'BRI', 150000, '2021-06-18', 'batik-apps.png', 'pending'),
(3, 1, 5, 'Mandiri', 1230000, '2021-06-18', 'social-apps.png', 'pending'),
(4, 5, 5, 'BRI', 1230000, '2021-06-19', '', 'pending'),
(5, 5, 4, 'Mandiri', 432333, '2021-06-19', 'chatbot1.jpeg', 'pending'),
(6, 6, 5, 'BRI', 150000, '2021-06-19', 'chatbot2.jpeg', 'sucsess');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`) VALUES
(2, 'relawan 2', 'relawan@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'relawan'),
(3, 'Ketua Divisi PPG', 'ppg@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ppg'),
(4, 'kewirausahaan', 'kewirausahaan@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'kewirausahaan'),
(5, 'ketua_relawan', 'ketua_relawan@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ketua_relawan'),
(7, 'nikki rufiansya pendonatur', 'nikki@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'donatur'),
(9, 'taufiq', 'taufiq@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'donatur'),
(10, 'sang pendonatur', 'donatur2@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'donatur'),
(14, 'nikki relawan', 'nikki_relawan@mail.com', 'ac43724f16e9241d990427ab7c8f4228', 'relawan'),
(15, 'dony_relawan', 'donny_relawan@mail.com', '7a814bce2fd4ad89fd470ddae9aff4d1', 'relawan'),
(17, 'Relawan A', 'relawan_a@mail.com', '999fbd096058db072f556783e16c5cc7', 'relawan'),
(18, 'relawan b ', 'relawan_b@mail.com', '6488e954d79ab1bdf32b5394d0fb92a5', 'relawan'),
(19, 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(21, 'dini andini', 'dini@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'kewirausahaan'),
(23, 'dika', 'dika@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ppg'),
(24, 'diki ', 'diki@mail.com', '43b93443937ea642a9a43e77fd5d8f77', 'ketua_relawan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokumentasi`
--
ALTER TABLE `dokumentasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donatur`
--
ALTER TABLE `donatur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donatur_tiap_kegiatan`
--
ALTER TABLE `donatur_tiap_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relawan`
--
ALTER TABLE `relawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relawan_tiap_kegiatan`
--
ALTER TABLE `relawan_tiap_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_kegiatan`
--
ALTER TABLE `status_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_barang`
--
ALTER TABLE `transaksi_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_uang`
--
ALTER TABLE `transaksi_uang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokumentasi`
--
ALTER TABLE `dokumentasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `donatur`
--
ALTER TABLE `donatur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `donatur_tiap_kegiatan`
--
ALTER TABLE `donatur_tiap_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `relawan`
--
ALTER TABLE `relawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `relawan_tiap_kegiatan`
--
ALTER TABLE `relawan_tiap_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `status_kegiatan`
--
ALTER TABLE `status_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi_barang`
--
ALTER TABLE `transaksi_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi_uang`
--
ALTER TABLE `transaksi_uang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
