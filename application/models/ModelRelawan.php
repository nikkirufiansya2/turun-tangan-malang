<?php

class ModelRelawan extends CI_Model
{

	function getAllData($table)
	{
		return $this->db->get($table);
	}

	function getData($table, $where)
	{
		$query = $this->db->get_where($table, $where);
		$query = $query->result_array();
		return $query;
	}

	public function relawanKegiatan($id)
	{
		return $this->db->query("SELECT a.id,a.judul,a.status_kegiatan,a.pesan_ajakan,a.deksripsi,a.minimal_relawan,a.minimal_donasi,a.tanggal,a.foto_kegiatan,a.laporan_dana FROM kegiatan a, relawan_tiap_kegiatan b, relawan c WHERE a.id = b.id_kegiatan AND b.id_relawan = c.id AND b.id_relawan ='$id'");
	}

	function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}


	function insert($data, $table)
	{
		$query = $this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function delete($table, $where)
	{
		$this->db->where($where);
		$this->db->delete($table);
		return TRUE;
	}

	public function relawanDonatur()
	{
		return $this->db->query('SELECT k.nama, d.email, d.alamat, d.no_hp FROM donatur_tiap_kegiatan k, donatur d WHERE d.id = k.id_donatur GROUP BY id_kegiatan');
	}
}
