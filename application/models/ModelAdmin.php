<?php

class ModelAdmin extends CI_Model
{
	function getAllData($table)
	{
		return $this->db->get($table);
	}

	function insert($data, $table)
	{
		$query = $this->db->insert($table, $data);
		return $query;
	}

	function getData($table, $where)
	{
		$query = $this->db->get_where($table, $where);
		$query = $query->result_array();
		return $query;
	}

	function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function delete($table, $where)
	{
		$this->db->where($where);
		$this->db->delete($table);
		return TRUE;
	}

}
