<?php

class ModelDonatur extends CI_Model
{

	function getAllData($table)
	{
		return $this->db->get($table);
	}

	function getData($table, $where)
	{
		$query = $this->db->get_where($table, $where);
		$query = $query->result_array();
		return $query;
	}

	function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function uploadBukti($id, $file)
	{
		$this->db->query("UPDATE `transaksi_uang` SET `bukti_transfer` = '$file' WHERE `transaksi_uang`.`id` = '$id'");
	}


	function insertData($data, $table)
	{
		$query = $this->db->insert($table, $data);
		return $query;
	}

	function verifikasi($id)
	{
		$this->db->query("UPDATE `transaksi_uang` SET `status` = 'sucsess' WHERE `transaksi_uang`.`id` = '$id'");
	}

	function verifikasiBarang($id)
	{
		$this->db->query("UPDATE `transaksi_barang` SET `status` = 'sucsess' WHERE `transaksi_barang`.`id` = '$id'");
	}

	function jumlahDonasi()
	{
		return $this->db->query("SELECT d.id, d.nama, d.email, t.jumlah_donasi FROM donatur d, transaksi_uang t WHERE d.id = t.id_donatur");
	}

	function laporanKeuangan($id)
	{
		return $this->db->query("SELECT k.judul, k.laporan_dana, k.file_laporan, t.id_donatur FROM kegiatan k, transaksi_uang t WHERE k.id = t.id_kegiatan AND t.id_donatur = '$id'");
	}
}
