<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Donatur extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelDonatur');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		$this->load->library('upload');

	}

	public function index()
	{
		$id_users = array('id_users' => $this->session->userdata("id_users"));
		$id_donatur = array('id_donatur' => $this->session->userdata("id_donatur"));
		$donatur['donatur'] = $this->ModelDonatur->getData("donatur", $id_users);
		$donatur['transaksi'] = $this->ModelDonatur->getData('transaksi_uang', $id_donatur);
		$donatur['transaksi_barang'] = $this->ModelDonatur->getData('transaksi_barang', $id_donatur);
		$donatur['transaksi_ambil_barang'] = $this->ModelDonatur->getData('transaksi_ambil_barang', $id_donatur);
		$donatur['laporan_keuangan'] = $this->ModelDonatur->laporanKeuangan($this->session->userdata("id_donatur"))->result();
		$this->load->view('donatur/head.php');
		$this->load->view('donatur/sidebar.php', $donatur);
		$this->load->view('donatur/index.php', $donatur);
		$this->load->view('donatur/footer.php');
	}

	public function ubahPassword()
	{
		$id_users = $this->input->post('id_users');
		$password = $this->input->post('password');
		$dataUsers = array(
			'password' => md5($password),
		);
		$where = array(
			'id' => $id_users
		);
		$this->ModelDonatur->update_data($where, $dataUsers, 'users');
		redirect(base_url("index.php/donatur"));
	}

	public function update()
	{
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$email = $this->input->post('email');
		$foto_old = $this->input->post('foto_old');
		$config['upload_path'] = './assets/foto/donatur';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['foto_donatur']['name'];
		$this->upload->initialize($config);

		if (!empty($_FILES['foto_donatur']['name'])) {
			if ($this->upload->do_upload('foto_donatur')) {
				$foto = $this->upload->data();
				$dataUsers = array(
					'name' => $nama,
					'email' => $email,
				);

				$where = array(
					'id' => $id
				);

				$this->ModelDonatur->update_data($where, $dataUsers, 'users');

				$dataDonatur = array(
					'email' => $email,
					'nama' => $nama,
					'alamat' => $alamat,
					'no_hp' => $no_hp,
					'foto' => $foto['file_name'],
				);

				$where2 = array(
					'id_users' => $id
				);
				$check = $this->ModelDonatur->update_data($where2, $dataDonatur, 'donatur');

				redirect(base_url("index.php/donatur"));
			} else {
				die('gagal upload');
			}
		} else {
			$dataUsers = array(
				'name' => $nama,
				'email' => $email,
			);

			$where = array(
				'id' => $id
			);

			$this->ModelDonatur->update_data($where, $dataUsers, 'users');

			$dataDonatur = array(
				'email' => $email,
				'nama' => $nama,
				'alamat' => $alamat,
				'no_hp' => $no_hp,
				'foto' => $foto_old,
			);

			$where2 = array(
				'id_users' => $id
			);
			$check = $this->ModelDonatur->update_data($where2, $dataDonatur, 'donatur');

			redirect(base_url("index.php/donatur"));
		}
	}


	public function donasiUang()
	{
		$id_kegiatan = $this->input->post('id_kegiatan');
		$id_donatur = $this->input->post('id_donatur');
		$nominal = $this->input->post('nominal');
		$bank = $this->input->post('bank');
		$data = array(
			'id_kegiatan' => $id_kegiatan,
			'id_donatur' => $id_donatur,
			'bank' => $bank,
			'jumlah_donasi' => $nominal,
			'waktu_donasi' => date("Y-m-d"),
			'status' => 'pending'

		);

		$this->ModelDonatur->insertData($data, 'transaksi_uang');
		$this->load->view('keterangan-transaksi.php');
	}

	public function prosesGabungRelawan()
	{
		$id_kegiatan = $this->input->post('id_kegiatan');
		$id_donatur = array('id' => $this->input->post('id_donatur'));
		$donatur = $this->ModelDonatur->getData('donatur', $id_donatur);
		foreach ($donatur as $key) ;
		$data = array(
			'id_kegiatan' => $id_kegiatan,
			'id_donatur' => $this->input->post('id_donatur'),
			'nama' => $key['nama'],
			'status' => 'bergabung'
		);
		$this->ModelDonatur->insertData($data, 'donatur_tiap_kegiatan');
		redirect(base_url("index.php/"));


	}


	public function donasiBarang()
	{
		$id_kegiatan = $this->input->post('id_kegiatan');
		$id_donatur = $this->input->post('id_donatur');
		$deskripsi = $this->input->post('deskripsi');
		$alamat = $this->input->post('alamat');
		$jasa = $this->input->post('jasa_kirim');
		$no_resi = $this->input->post('no_resi');
		$data = array(
			'id_kegiatan' => $id_kegiatan,
			'id_donatur' => $id_donatur,
			'deskripsi' => $deskripsi,
			'alamat' => $alamat,
			'jasa_kirim' => $jasa,
			'no_resi' => $no_resi,
			'waktu_donasi' => date("Y-m-d"),
			'status' => 'pending'
		);

		$this->ModelDonatur->insertData($data, 'transaksi_barang');
		$this->load->view('keterangan-transaksi-barang.php');
	}

	public function donasiAntarBarang()
	{
		$id_kegiatan = $this->input->post('id_kegiatan');
		$id_donatur = $this->input->post('id_donatur');
		$data = array(
			'id_kegiatan' => $id_kegiatan,
			'id_donatur' => $id_donatur,
			'nama_barang' => $this->input->post('nama_barang'),
			'alamat' => $this->input->post('alamat'),
		);

		$this->ModelDonatur->insertData($data, 'transaksi_ambil_barang');
		$this->load->view('keterangan-transaksi-barang.php');

	}

	public function uploadBukti()
	{
		$id = $this->input->post('id');
		$config['upload_path'] = './assets/foto/bukti';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['bukti_transfer']['name'];
		$this->upload->initialize($config);
		if (!empty($_FILES['bukti_transfer']['name'])) {
			if ($this->upload->do_upload('bukti_transfer')) {
				$foto = $this->upload->data();
				$this->ModelDonatur->uploadBukti($id, $foto['file_name']);
				redirect(base_url("index.php/donatur"));
			}
		}
	}

	public function downloadFile($file)
	{
		$this->load->helper('download');
		$name = './assets/file/'.$file;
		force_download($name, null);
	}
}
