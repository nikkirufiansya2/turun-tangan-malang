<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelLogin');
		$this->load->model('ModelRegister');
		$this->load->model('ModelDonatur');
		$this->load->model('ModelKegiatan');
		$this->load->helper('url');
		$this->load->library('session');

	}

	public function index()
	{
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getAllData('kegiatan')->result();
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('index.php', $kegiatan);
	}

	public function kegiatan()
	{
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getAllData('kegiatan')->result();
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('kegiatan.php', $kegiatan);
	}

	public function informasiKegiatan($id)
	{
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$kegiatan['relawan'] = $this->ModelKegiatan->relawanKegiatan($id)->result();
		$kegiatan['dokumentasi'] = $this->ModelKegiatan->getFotoKegiatan($id)->result();
		$kegiatan['komentar'] = $this->ModelKegiatan->getKomentar($id)->result();
		$kegiatan['donatur'] = $this->ModelKegiatan->getDonaturKegiatan($id)->result();
		$kegiatan['kegiatan_donatur'] = $this->ModelKegiatan->kegiatanDonatur($id)->result();
		$kegiatan['gabung'] = $this->ModelKegiatan->statusGabung( $this->session->userdata("id_donatur"), $id)->result();
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('informasi-kegiatan.php', $kegiatan);
	}

	public function komentarKegiatan()
	{
		$id = $this->input->post('id');
		$id_donatur = $this->input->post('id_donatur');
		$komentar = $this->input->post('komentar');
		$data = array(
			'id_kegiatan' => $id,
			'id_donatur' => $id_donatur,
			'isi_komentar' => $komentar
		);
		$this->ModelKegiatan->insert($data, 'komentar');
		redirect(base_url(''));
	}


	public function donasiUang($id)
	{
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('form-donasi-uang.php', $kegiatan);
	}

	public function donasiBarang($id)
	{
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('form-donasi-barang.php', $kegiatan);
	}

	public function antarBarang($id)
	{
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('form-donasi-antar-barang.php', $kegiatan);
	}

	public function metodeDonasiBarang($id)
	{
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('metode-donasi-barang.php', $kegiatan);
	}

	public function gabungRelawan($id)
	{
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$this->load->view('head.php');
		$this->load->view('navbar.php');
		$this->load->view('gabung-relawan.php', $kegiatan);
	}

	public function formLogin()
	{
		$this->load->view('form-login.php');
	}

	public function login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$where = array(
			'email' => $email,
			'password' => md5($password),
		);

		$check = $this->ModelLogin->cek_login("users", $where);
		if (count($check) == 1) {
			foreach ($check as $key) ;
			$id_users = array('id_users' => $key['id']);
			$level = $key['level'];
			if ($level == 'donatur') {
				$id_users = array('id_users' => $key['id']);
				$donatur = $this->ModelKegiatan->getData('donatur', $id_users);
				foreach ($donatur as $donatur) ;
				$data_session = array(
					'nama' => $key['name'],
					'id_users' => $key['id'],
					'id_donatur' => $donatur['id'],
					'status' => "login"
				);
				$this->session->set_userdata($data_session);
				redirect(base_url("index.php/donatur"));
			} else if ($level == 'relawan') {
				$relawan = $this->ModelKegiatan->getData('relawan', $id_users);
				foreach ($relawan as $relawan) ;
				$data_session = array(
					'nama' => $key['name'],
					'id_users' => $key['id'],
					'id_relawan' => $relawan['id'],
					'status' => "login"
				);
				$this->session->set_userdata($data_session);
				redirect(base_url("index.php/relawan"));
			} else if ($level == 'ppg') {
				$data_session = array(
					'nama' => $key['name'],
					'id_users' => $key['id'],
					'status' => "login"
				);
				$this->session->set_userdata($data_session);
				redirect(base_url("index.php/ppg"));
			} else if ($level == 'kewirausahaan') {
				$data_session = array(
					'nama' => $key['name'],
					'id_users' => $key['id'],
					'status' => "login"
				);
				$this->session->set_userdata($data_session);
				redirect(base_url("index.php/kewirausahaan"));
			} else if ($level == 'ketua_relawan') {
				$data_session = array(
					'nama' => $key['name'],
					'id_users' => $key['id'],
					'status' => "login"
				);
				$this->session->set_userdata($data_session);
				redirect(base_url('index.php/relawan/Relawan/ketuaRelawan'));
			} else if ($level == 'admin') {
				$data_session = array(
					'nama' => $key['name'],
					'status' => "login"
				);
				$this->session->set_userdata($data_session);
				redirect(base_url('index.php/admin'));
			}

		} else {
			echo "login gagal";

		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('index.php/dashboard'));
	}

	public function formRegister()
	{
		$this->load->view('form-register.php');
	}

	public function register()
	{
		$name = $this->input->post('nama');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('nohp');

		$dataUsers = array(
			'name' => $name,
			'email' => $email,
			'password' => md5($password),
			'level' => 'donatur'
		);

		$id_users = $this->ModelRegister->registerDonatur($dataUsers, 'users');

		$dataDonatur = array(
			'email' => $email,
			'nama' => $name,
			'alamat' => $alamat,
			'no_hp' => $no_hp,
			'id_users' => $id_users
		);

		$check = $this->ModelRegister->registerDonatur($dataDonatur, 'donatur');

		if ($check > 0) {
			$this->load->view('form-login');
		}

	}
}
