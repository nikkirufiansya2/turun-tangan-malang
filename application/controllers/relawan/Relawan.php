<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Relawan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelRelawan');
		$this->load->model('ModelAdmin');
		$this->load->model('ModelKegiatan');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		$this->load->library('upload');

	}

	public function index()
	{
		$id_users = array('id_users' => $this->session->userdata("id_users"));
		$id = array('id' => $this->session->userdata("id_users"));
		$id_relawan = $this->session->userdata("id_relawan");
		$relawan['relawan'] = $this->ModelRelawan->getData('relawan', $id_users);
		$relawan['users'] = $this->ModelRelawan->getData('users', $id);
		$relawan['kegiatan'] = $this->ModelRelawan->relawanKegiatan($id_relawan)->result();
		$this->load->view('relawan/head.php');
		$this->load->view('relawan/sidebar.php', $relawan);
		$this->load->view('relawan/index.php', $relawan);
		$this->load->view('relawan/footer.php');
	}


	public function ketuaRelawan()
	{
		$relawan['relawan'] = $this->ModelRelawan->getAllData('relawan')->result();
		$relawan['donatur'] = $this->ModelRelawan->relawanDonatur()->result();
		$this->load->view('ketua-relawan/head.php');
		$this->load->view('ketua-relawan/sidebar.php');
		$this->load->view('ketua-relawan/index.php', $relawan);
		$this->load->view('ketua-relawan/footer.php');
	}


	public function tambahRelawan()
	{
		$nama = $this->input->post('nama');
		$divisi = $this->input->post('divisi');
		$profesi = $this->input->post('profesi');
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$id_line = $this->input->post('id_line');
		$email = $this->input->post('email');

		// get foto
		$config['upload_path'] = './assets/foto/relawan';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['foto_relawan']['name'];
		$this->upload->initialize($config);

		if (!empty($_FILES['foto_relawan']['name'])) {
			if ($this->upload->do_upload('foto_relawan')) {
				$foto = $this->upload->data();
				$dataUsers = array(
					'name' => $nama,
					'email' => $email,
					'password' => md5($email),
					'level' => 'relawan'
				);

				$id_users = $this->ModelRelawan->insert($dataUsers, 'users');


				$data = array(
					'id_users' => $id_users,
					'nama' => $nama,
					'divisi' => $divisi,
					'profesi' => $profesi,
					'fakultas' => $fakultas,
					'jurusan' => $jurusan,
					'alamat' => $alamat,
					'no_hp' => $no_hp,
					'id_line' => $id_line,
					'email' => $email,
					'foto' => $foto['file_name']
				);

				$this->ModelRelawan->insert($data, 'relawan');
				redirect(base_url('index.php/relawan/Relawan/ketuaRelawan'));
			} else {
				die("gagal upload");
			}
		} else {
			echo "tidak masuk";
		}
	}

	public function ubahPassword()
	{
		$id_users = $this->input->post('id_users');
		$password = $this->input->post('password');
		$dataUsers = array(
			'password' => md5($password),
		);
		$where = array(
			'id' => $id_users
		);
		$this->ModelRelawan->update_data($where, $dataUsers, 'users');
		redirect(base_url('index.php/relawan/Relawan/'));
	}


	public function ubahRelawan()
	{
		$id = $this->input->post('id');
		$id_users = $this->input->post('id_users');
		$nama = $this->input->post('nama');
		$divisi = $this->input->post('divisi');
		$profesi = $this->input->post('profesi');
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$id_line = $this->input->post('id_line');
		$email = $this->input->post('email');
		$foto_old = $this->input->post('foto_old');
		// get foto
		$config['upload_path'] = './assets/foto/relawan';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['foto_relawan']['name'];
		$this->upload->initialize($config);

		if (!empty($_FILES['foto_relawan']['name'])) {
			if ($this->upload->do_upload('foto_relawan')) {
				$foto = $this->upload->data();
				$dataUsers = array(
					'name' => $nama,
					'email' => $email,

				);
				$where = array(
					'id' => $id_users
				);
				$this->ModelRelawan->update_data($where, $dataUsers, 'users');


				$data = array(
					'nama' => $nama,
					'divisi' => $divisi,
					'profesi' => $profesi,
					'fakultas' => $fakultas,
					'jurusan' => $jurusan,
					'alamat' => $alamat,
					'no_hp' => $no_hp,
					'id_line' => $id_line,
					'email' => $email,
					'foto' => $foto['file_name']
				);

				$where2 = array(
					'id' => $id
				);

				$this->ModelRelawan->update_data($where2, $data, 'relawan');
				redirect(base_url('index.php/relawan/Relawan/'));
			} else {
				die("gagal upload");
			}
		} else {
			$dataUsers = array(
				'name' => $nama,
				'email' => $email,
			);


			$where = array(
				'id' => $id_users
			);

			$this->ModelRelawan->update_data($where, $dataUsers, 'users');


			$data = array(
				'nama' => $nama,
				'divisi' => $divisi,
				'profesi' => $profesi,
				'fakultas' => $fakultas,
				'jurusan' => $jurusan,
				'alamat' => $alamat,
				'no_hp' => $no_hp,
				'id_line' => $id_line,
				'email' => $email,
				'foto' => $foto_old
			);

			$where2 = array(
				'id' => $id
			);

			$this->ModelRelawan->update_data($where2, $data, 'relawan');
			redirect(base_url('index.php/relawan/Relawan/'));
		}

	}

	public function deletedata($id, $id_users, $foto)
	{
		$path = './assets/foto/relawan';
		@unlink($path . $foto);

		$where = array('id' => $id);
		$this->ModelRelawan->delete('relawan', $where);
		$where2 = array('id' => $id_users);
		$this->ModelRelawan->delete('users', $where2);
		redirect(base_url('index.php/relawan/Relawan/ketuaRelawan'));
	}


	public function detailRelawan($id)
	{
		$idRelawan = array('id' => $id);
		$relawan['relawan'] = $this->ModelRelawan->getData('relawan', $idRelawan);
		$relawan['kegiatan'] = $this->ModelKegiatan->kegiatanRelawan($id)->result();
		$this->load->view('ketua-relawan/head.php');
		$this->load->view('ketua-relawan/sidebar.php');
		$this->load->view('ketua-relawan/detail-relawan.php', $relawan);
		$this->load->view('ketua-relawan/footer.php');
	}


	public function detailKegiatan($id)
	{
		$id_users = array('id_users' => $this->session->userdata("id_users"));
		$kegiatan['relawan'] = $this->ModelRelawan->getData('relawan', $id_users);
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelRelawan->getData('kegiatan', $id_kegiatan);
		$this->load->view('relawan/head.php');
		$this->load->view('relawan/sidebar.php', $kegiatan);
		$this->load->view('relawan/detail-kegiatan.php', $kegiatan);
		$this->load->view('relawan/footer.php');
	}

	public function pemberitahuan($id)
	{
		$id_users = array('id_users' => $this->session->userdata("id_users"));
		$pemberitahuan['relawan'] = $this->ModelRelawan->getData('relawan', $id_users);
		$id_kegiatan = array('id_kegiatan' => $id);
		$pemberitahuan['pemberitahuan'] = $this->ModelRelawan->getData('pemberitahuan', $id_kegiatan);
		$this->load->view('relawan/head.php');
		$this->load->view('relawan/sidebar.php', $pemberitahuan);
		$this->load->view('relawan/pemberitahuan.php', $pemberitahuan);
		$this->load->view('relawan/footer.php');
	}


	public function profile($id)
	{
		$id = array('id' => $id);
		$users['users'] = $this->ModelAdmin->getData('users', $id);
		$this->load->view('ketua-relawan/head.php');
		$this->load->view('ketua-relawan/sidebar.php');
		$this->load->view('ketua-relawan/update-profile.php', $users);
		$this->load->view('ketua-relawan/footer.php');
	}

	public function prosesUpdateUsers()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$oldPassowrd = $this->input->post('oldPassowrd');
		$newPassword = $this->input->post('password');

		if ($newPassword == null) {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => $oldPassowrd

			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/relawan/Relawan/ketuaRelawan'));
		} else {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => md5($newPassword)

			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/relawan/Relawan/ketuaRelawan'));
		}

	}

}
