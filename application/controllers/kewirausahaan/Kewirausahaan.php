<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kewirausahaan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelDonatur');
		$this->load->model('ModelKegiatan');
		$this->load->model('ModelAdmin');
		$this->load->helper('url');

	}

	public function index()
	{
		$transaksi['transaksi'] = $this->ModelKegiatan->transaksi()->result();
		$transaksi['transaksi_barang'] = $this->ModelKegiatan->transaksiBarang()->result();
		$this->load->view('kewirausahaan/head.php');
		$this->load->view('kewirausahaan/sidebar.php');
		$this->load->view('kewirausahaan/index.php', $transaksi);
		$this->load->view('kewirausahaan/footer.php');

	}

	public function kelolaDonatur()
	{
		$donatur['donatur'] = $this->ModelDonatur->getAllData('donatur')->result();
		$this->load->view('kewirausahaan/head.php');
		$this->load->view('kewirausahaan/sidebar.php');
		$this->load->view('kewirausahaan/kelola-donatur.php', $donatur);
		$this->load->view('kewirausahaan/footer.php');
	}


	public function verifikasi($id)
	{
		$this->ModelDonatur->verifikasi($id);
		redirect(base_url('index.php/kewirausahaan'));
	}


	public function verifikasiBarang($id)
	{
		$this->ModelDonatur->verifikasiBarang($id);
		redirect(base_url('index.php/kewirausahaan'));
	}

	public function delete($id)
	{
		$idTransaksi = array('id' => $id);
		$this->ModelKegiatan->delete('transaksi_uang', $idTransaksi);
		redirect(base_url('index.php/kewirausahaan'));
	}

	public function deleteTransaksiBarang($id)
	{
		$idTransaksi = array('id' => $id);
		$this->ModelKegiatan->delete('transaksi_barang', $idTransaksi);
		redirect(base_url('index.php/kewirausahaan'));
	}

	public function profile($id)
	{
		$id = array('id' => $id);
		$users['users'] = $this->ModelAdmin->getData('users', $id);
		$this->load->view('kewirausahaan/head.php');
		$this->load->view('kewirausahaan/sidebar.php');
		$this->load->view('kewirausahaan/update-profile.php', $users);
		$this->load->view('kewirausahaan/footer.php');
	}

	public function prosesUpdateUsers()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$oldPassowrd = $this->input->post('oldPassowrd');
		$newPassword = $this->input->post('password');

		if ($newPassword == null) {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => $oldPassowrd

			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/kewirausahaan'));
		} else {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => md5($newPassword)

			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/kewirausahaan'));
		}

	}

	public function detailDonatur($id){
		$id = array('id' => $id);
		$users['users'] = $this->ModelAdmin->getData('donatur', $id);
		$this->load->view('kewirausahaan/head.php');
		$this->load->view('kewirausahaan/sidebar.php');
		$this->load->view('kewirausahaan/detail-donatur.php', $users);
		$this->load->view('kewirausahaan/footer.php');	
	}


}
