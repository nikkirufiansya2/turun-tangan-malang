<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelRelawan');
		$this->load->model('ModelAdmin');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		$this->load->library('upload');

	}

	public function index()
	{
		$users['users'] = $this->ModelAdmin->getAllData('users')->result();
		$this->load->view('admin/head.php');
		$this->load->view('admin/sidebar.php');
		$this->load->view('admin/index.php', $users);
		$this->load->view('admin/footer.php');
	}

	public function tambahUsers()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$level = $this->input->post('level');

		$dataUsers = array(
			'name' => $name,
			'email' => $email,
			'password' => md5($email),
			'level' => $level
		);

		$this->ModelAdmin->insert($dataUsers, 'users');
		redirect(base_url('index.php/admin/'));
	}

	public function updateUser($id)
	{
		$id = array('id' => $id);
		$users['users'] = $this->ModelAdmin->getData('users', $id);
		$this->load->view('admin/head.php');
		$this->load->view('admin/sidebar.php');
		$this->load->view('admin/update-users.php', $users);
		$this->load->view('admin/footer.php');
	}

	public function prosesUpdateUsers()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$level = $this->input->post('level');
		$oldPassowrd = $this->input->post('oldPassowrd');
		$newPassword = $this->input->post('password');


		if ($newPassword == null) {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => $oldPassowrd,
				'level' => $level
			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/admin/'));
		} else {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => md5($newPassword),
				'level' => $level
			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/admin/'));
		}


	}

	public function deleteData($id)
	{
		$where = array('id' => $id);
		$this->ModelAdmin->delete('users', $where);
		redirect(base_url('index.php/admin/'));
	}


}
