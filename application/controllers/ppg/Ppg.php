<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ppg extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModelKegiatan');
		$this->load->model('ModelRelawan');
		$this->load->model('ModelAdmin');
		$this->load->helper(array('form', 'url'));
		$this->load->library('upload');
		$this->load->library('session');

	}

	public function index()
	{
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getKegiatan('kegiatan', 10);
		$kegiatan['relawan'] = $this->ModelRelawan->getAllData('relawan')->result();
		$this->load->view('ppg/head.php');
		$this->load->view('ppg/sidebar.php');
		$this->load->view('ppg/index.php', $kegiatan);
		$this->load->view('ppg/footer.php');
	}

	public function tambahKegiatan()
	{
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getAllData('status_kegiatan')->result();
		$this->load->view('ppg/head.php');
		$this->load->view('ppg/sidebar.php');
		$this->load->view('ppg/tambah-kegiatan.php', $kegiatan);
		$this->load->view('ppg/footer.php');
	}

	public function updateKegiatan($id)
	{
		$kegiatan['status_kegiatan'] = $this->ModelKegiatan->getAllData('status_kegiatan')->result();
		$id_kegiatan = array('id' => $id);
		$kegiatan['kegiatan'] = $this->ModelKegiatan->getData('kegiatan', $id_kegiatan);
		$this->load->view('ppg/head.php');
		$this->load->view('ppg/sidebar.php');
		$this->load->view('ppg/update-kegiatan.php', $kegiatan);
		$this->load->view('ppg/footer.php');
	}

	public function editKegiatan()
	{
		$id = $this->input->post('id');
		$id_kegiatan = array('id' => $id);
		$judul = $this->input->post('nama_kegiatan');
		$status = $this->input->post('status_kegiatan');
		$pesan = $this->input->post('pesan_ajakan');
		$deskripsi = $this->input->post('deskripsi');
		$min_relawan = $this->input->post('min_relawan');
		$min_donasi = $this->input->post('min_donasi');
		$tanggal = $this->input->post('tanggal_kegiatan');
		$foto_old = $this->input->post('foto_old');
		$alamat = $this->input->post('alamat');
		// get foto
		$config['upload_path'] = './assets/foto/kegiatan';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['foto_kegiatan']['name'];
		$this->upload->initialize($config);

		if (!empty($_FILES['foto_kegiatan']['name'])) {
			if ($this->upload->do_upload('foto_kegiatan')) {
				$foto = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'status_kegiatan' => $status,
					'pesan_ajakan' => $pesan,
					'deksripsi' => $deskripsi,
					'minimal_relawan' => $min_relawan,
					'minimal_donasi' => $min_donasi,
					'tanggal' => $tanggal,
					'foto_kegiatan' => $foto['file_name'],
					'alamat' => $alamat
				);
				$this->ModelKegiatan->update_data($id_kegiatan, $data, 'kegiatan');
				redirect(base_url('index.php/ppg/Ppg/'));
			} else {
				echo "gagal update";
			}
		} else {
			$data = array(
				'judul' => $judul,
				'status_kegiatan' => $status,
				'pesan_ajakan' => $pesan,
				'deksripsi' => $deskripsi,
				'minimal_relawan' => $min_relawan,
				'minimal_donasi' => $min_donasi,
				'tanggal' => $tanggal,
				'foto_kegiatan' => $foto_old,
				'alamat' => $alamat
			);
			$this->ModelKegiatan->update_data($id_kegiatan, $data, 'kegiatan');
			redirect(base_url('index.php/ppg/Ppg/'));
		}

	}

	public function addKegiatan()
	{
		$judul = $this->input->post('nama_kegiatan');
		$status = $this->input->post('status_kegiatan');
		$pesan = $this->input->post('pesan_ajakan');
		$deskripsi = $this->input->post('deskripsi');
		$min_relawan = $this->input->post('min_relawan');
		$min_donasi = $this->input->post('min_donasi');
		$tanggal = $this->input->post('tanggal_kegiatan');
		$alamat = $this->input->post('alamat');
		// get foto
		$config['upload_path'] = './assets/foto/kegiatan';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['foto_kegiatan']['name'];
		$this->upload->initialize($config);

		if (!empty($_FILES['foto_kegiatan']['name'])) {
			if ($this->upload->do_upload('foto_kegiatan')) {
				$foto = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'status_kegiatan' => $status,
					'pesan_ajakan' => $pesan,
					'deksripsi' => $deskripsi,
					'minimal_relawan' => $min_relawan,
					'minimal_donasi' => $min_donasi,
					'tanggal' => $tanggal,
					'foto_kegiatan' => $foto['file_name'],
					'alamat' => $alamat
				);

				$this->ModelKegiatan->insert($data, 'kegiatan');
				redirect(base_url('index.php/ppg/Ppg/'));
			}
		}

	}

	public function komentarKegiatan($id)
	{
		$id_kegiatan = array('id_kegiatan' => $id);
		$komentar['komentar'] = $this->ModelKegiatan->getDataById('komentar', $id)->result();
		$this->load->view('ppg/head.php');
		$this->load->view('ppg/sidebar.php');
		$this->load->view('ppg/komentar-kegiatan.php', $komentar);
		$this->load->view('ppg/footer.php');
	}

	public function deleteKomentar($id)
	{
		$id_kegiatan = array('id' => $id);
		$this->ModelKegiatan->delete('komentar', $id_kegiatan);
		redirect(base_url('index.php/ppg/Ppg/'));
	}

	public function delete($id)
	{
		$id_kegiatan = array('id' => $id);
		$this->ModelKegiatan->delete('kegiatan', $id_kegiatan);
		redirect(base_url('index.php/ppg/Ppg/'));
	}

	public function updateLaporanDana()
	{
		$id = $this->input->post('id_kegiatan');
		$laporan_dana = $this->input->post('laporan_dana');
		$config['allowed_types'] = 'txt|csv|xlsx|xls';
		$config['upload_path'] = './assets/file/';
		$config['overwrite'] = true;
		$config['file_name'] = $_FILES['file_laporan']['name'];
		$this->upload->initialize($config);
		if (!empty($_FILES['file_laporan']['name'])) {
			if ($this->upload->do_upload('file_laporan')) {
				$file = $this->upload->data();
				$data = array(
					'laporan_dana' => $laporan_dana,
					'file_laporan' => $file['file_name']
				);
				$id_kegiatan = array('id' => $id);
				$this->ModelKegiatan->update_data($id_kegiatan, $data, 'kegiatan');
				redirect(base_url('index.php/ppg/Ppg/'));
			}else{
				$error = array('error' => $this->upload->display_errors());
				print_r($error);
			}
		}
	}

	public function uploadPemberitahuan()
	{
		$id = $this->input->post('id_kegiatan');
		$pemberitahuan = $this->input->post('pemberitahuan');
		$data = array(
			'id_kegiatan' => $id,
			'pesan' => $pemberitahuan,
			'tanggal' => date("Y-m-d")
		);
		$this->ModelKegiatan->insert($data, 'pemberitahuan');
		redirect(base_url('index.php/ppg/Ppg/'));

	}

	public function uploadDokumentasi()
	{
		$id = $this->input->post('id_kegiatan');
		// get foto
		$config['upload_path'] = './assets/foto/dokumentasi';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['foto_kegiatan']['name'];

		$this->upload->initialize($config);

		if (!empty($_FILES['foto_kegiatan']['name'])) {
			if ($this->upload->do_upload('foto_kegiatan')) {
				$foto = $this->upload->data();
				$data = array(
					'id_kegiatan' => $id,
					'foto' => $foto['file_name']
				);

				$this->ModelKegiatan->insert($data, 'dokumentasi');
				redirect(base_url('index.php/ppg/Ppg/'));
			}
		}

	}

	public function kehadiranRelawan()
	{
		$id = $this->input->post('id_kegiatan');
		$id_relawan = array('id' => $this->input->post('relawan'));
		$dataRelawan = $this->ModelRelawan->getData('relawan', $id_relawan);
		foreach ($dataRelawan as $key) ;
		$data = array(
			'id_kegiatan' => $id,
			'id_relawan' => $key['id'],
			'nama_relawan' => $key['nama']
		);
		$this->ModelKegiatan->insertData($data, 'relawan_tiap_kegiatan');
		redirect(base_url('index.php/ppg/Ppg/'));
	}

	public function profile($id)
	{
		$id = array('id' => $id);
		$users['users'] = $this->ModelAdmin->getData('users', $id);
		$this->load->view('ppg/head.php');
		$this->load->view('ppg/sidebar.php');
		$this->load->view('ppg/update-profile.php', $users);
		$this->load->view('ppg/footer.php');
	}

	public function prosesUpdateUsers()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$oldPassowrd = $this->input->post('oldPassowrd');
		$newPassword = $this->input->post('password');

		if ($newPassword == null) {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => $oldPassowrd

			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/ppg/Ppg/'));
		} else {
			$dataUsers = array(
				'name' => $name,
				'email' => $email,
				'password' => md5($newPassword)

			);
			$where = array('id' => $id);
			$this->ModelAdmin->update_data($where, $dataUsers, 'users');
			redirect(base_url('index.php/ppg/Ppg/'));
		}

	}


}
