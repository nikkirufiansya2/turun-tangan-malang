<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-danger static-top">
	<div class="container">
		<a class="navbar-brand" href="<?php echo base_url();?>index.php/dashboard/">
			<img src="http://placehold.it/150x50?text=Logo" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo base_url();?>index.php/dashboard/">Home
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url();?>index.php/dashboard/kegiatan">Kegiatan</a>
				</li>

				<li class="nav-item">
					<?php
					if ($this->session->userdata("status") == "login") {
						?>
						<a class="nav-link" href="<?php echo base_url('index.php/donatur/Donatur'); ?>">Profile</a>
						<?php		
					}else{
						
					}
					?>
				</li>

				<li class="nav-item">
					<?php
					if ($this->session->userdata("status") == "login") {
						?>
						<a class="nav-link" href="<?php echo base_url('index.php/dashboard/logout'); ?>">Logout</a>
						<?php		
					}else{
						?>
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/dashboard/formLogin">Login</a>
						<?php
					}
					?>
				</li>
			</ul>
		</div>
	</div>
</nav>