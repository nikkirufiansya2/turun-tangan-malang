<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Super Admin</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard Super Admin</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data Users</h3>
			<?php foreach ($users

			as $key): ?>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<form action="<?php echo base_url('index.php/admin/Admin/prosesUpdateUsers'); ?>" method="post"
				  enctype="multipart/form-data">
				<div class="form-group">
					<label>Name</label>
					<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
					<input type="text" name="name" value="<?php echo $key['name'] ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="text" name="email" value="<?php echo $key['email'] ?>" class="form-control">
				</div>

				<div class="form-group">
					<label for="" class="col-sm-2 col-form-label">Level</label>
					<select class="form-control" name="level" style="margin-top: 10px;">
						<option><?php echo $key['level'] ?></option>
						<option>kewirausahaan</option>
						<option>ketua_relawan</option>
						<option>ppg</option>
					</select>
				</div>
				<div class="form-group">
					<label>New Password</label>
					<input type="hidden" name="oldPassowrd" value="<?php echo $key['password'] ?>">
					<input type="password" name="password" placeholder="masukan password baru" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" name="submit" class="btn btn-success"/>
				</div>
				<?php endforeach ?>
			</form>
		</div>

	</div>
	</div>
</section>

</body>
</html>
