<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Super Admin</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard Super Admin</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data User</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<label>Data Users</label><br>
			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#tambah-relawan" style="margin-bottom: 10px;">Tambah User</a>

			<table id="example2" class="table table-bordered table-hover">
				<thead>
				<tr>
					<th>Nama Relawan</th>
					<th>Email</th>
					<th>Level</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $key): ?>
				<tr>
					<td><?php echo $key->name?></td>
					<td><?php echo $key->email?></td>
					<td><?php echo $key->level?></td>
					<td>
						<a href="<?php echo base_url(); ?>index.php/admin/Admin/updateUser/<?= $key->id?>" class="btn btn-warning">Edit</a>
						<a href="<?php echo base_url(); ?>index.php/admin/Admin/deleteData/<?= $key->id?>" class="btn btn-danger">Hapus</a>
					</td>
				</tr>
				</tfoot>
				<?php endforeach ?>
			</table>
		</div>


		<!-- form tambah relawan -->
		<div class="modal fade" id="tambah-relawan" tabindex="-1" role="dialog" aria-labelledby="tambah-relawan" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<label>Form Tambah Users</label>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>

					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('index.php/admin/Admin/tambahUsers'); ?>"  method="post" enctype="multipart/form-data">
							<div class="form-group row">
								<label for="" class="col-sm-2 col-form-label">Nama</label>
								<div class="col-sm-10">
									<input type="text" name="name" class="form-control" id="">
								</div>
							</div>

							<div class="form-group row">
								<label for="" class="col-sm-2 col-form-label">Email</label>
								<div class="col-sm-10">
									<input type="email" name="email" class="form-control" id="">
								</div>
							</div>

							<div class="form-group row">
								<label for="" class="col-sm-2 col-form-label">Level</label>
								<div class="col-sm-10">
									<select class="form-control" name="level" style="margin-top: 10px;">
										<option>Pilih Level User</option>
										<option>kewirausahaan</option>
										<option>ketua_relawan</option>
										<option>ppg</option>
									</select>
								</div>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<input type="submit" name="submit" class="btn btn-success"/>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
		<!-- form tambah relawan -->
</body>
</html>
