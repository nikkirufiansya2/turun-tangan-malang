<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Ketua Relawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard Ketua Relawan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Detail Relawan</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <?php foreach ($relawan as $key): ?>


          <div class="card-body">
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="" name="nama" readonly="" value="<?php echo $key['nama']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Divisi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" readonly="" value="<?php echo $key['divisi']?>" id="">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Profesi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" readonly="" value="<?php echo $key['profesi']?>" id="">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Fakultas</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" readonly="" value="<?php echo $key['fakultas']?>" id="">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Jurusan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" readonly="" value="<?php echo $key['jurusan']?>" id="">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Alamat</label>
              <div class="col-sm-10">
                <textarea class="form-control" readonly="" ><?php echo $key['alamat']?></textarea> 
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">No Hp</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" readonly="" value="<?php echo $key['no_hp']?>" id="">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">ID Line</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" readonly="" value="<?php echo $key['id_line']?>" id="">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" readonly="" value="<?php echo $key['email']?>" id="">
              </div>
            </div>
          <?php endforeach ?>


          <label>Data Kontribusi Relawan</label>
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Nama Kegiatan</th>
                <th>Status Kegiatan</th>
              </tr>
            </thead>
            <tbody>
			<?php foreach($kegiatan as $key):?>
              <tr>
                <td><?php echo $key->judul?></td>
                <td><?php echo $key->status_kegiatan?></td>
              </tr>
			  <?php endforeach;?>
            </tfoot>
          </table>
        </div>



      </body>
      </html>
