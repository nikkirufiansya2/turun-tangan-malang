<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Ketua Relawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard Ketua Relawan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Relawan</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
         <label>Data Relawan</label><br>
          <a href="" class="btn btn-primary" data-toggle="modal" data-target="#tambah-relawan" style="margin-bottom: 10px;">Tambah Relawan</a>
           
         <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Nama Relawan</th>
              <th>Email</th>
              <th>Divisi</th>
              <th>Alamat</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($relawan as $key): ?>
            <tr>
              <td><?php echo $key->nama?></td>
              <td><?php echo $key->email?></td>
              <td><?php echo $key->divisi?></td>
              <td><?php echo $key->alamat?></td>
              <td>
                <a href="<?php echo base_url(); ?>index.php/relawan/Relawan/detailRelawan/<?= $key->id?>" class="btn btn-primary">Detail</a> 
                <a href="<?php echo base_url(); ?>index.php/relawan/Relawan/deletedata/<?= $key->id?>/<?= $key->id_users?>/<?= $key->foto?>" class="btn btn-danger">Hapus</a>
              </td>
            </tr>
          </tfoot>
           <?php endforeach ?>
        </table>
      </div>


		<div class="card">

			<div class="card-body">
				<label>Data Donatur Jadi Relawan</label><br>
				<table id="example2" class="table table-bordered table-hover">
					<thead>
					<tr>
						<th>Nama Relawan</th>
						<th>Email</th>
						<th>Alamat</th>
						<th>Telepone</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($donatur as $key): ?>
					<tr>
						<td><?php echo $key->nama?></td>
						<td><?php echo $key->email?></td>
						<td><?php echo $key->alamat?></td>
						<td><?php echo $key->no_hp?></td>

					</tr>
					</tfoot>
					<?php endforeach ?>
				</table>
			</div>


       <!-- form tambah relawan -->
      <div class="modal fade" id="tambah-relawan" tabindex="-1" role="dialog" aria-labelledby="tambah-relawan" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label>Form Tambah Relawan</label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>

            </div>
            <div class="modal-body">
              <form action="<?php echo base_url('index.php/relawan/Relawan/tambahRelawan'); ?>"  method="post" enctype="multipart/form-data">
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" name="nama" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Divisi</label>
                  <div class="col-sm-10">
                    <input type="text" name="divisi" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Profesi</label>
                  <div class="col-sm-10">
                    <input type="text" name="profesi" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Fakultas</label>
                  <div class="col-sm-10">
                    <input type="text" name="fakultas" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Jurusan</label>
                  <div class="col-sm-10">
                    <input type="text" name="jurusan" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Alamat</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="alamat"></textarea> 
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">No Hp</label>
                  <div class="col-sm-10">
                    <input type="text" name="no_hp" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">ID Line</label>
                  <div class="col-sm-10">
                    <input type="text" name="id_line" class="form-control" id="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" id="">
                  </div>
                </div>
                
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Foto</label>
                  <div class="col-sm-10">
                   <input type="file" name="foto_relawan">
                 </div>

               </div>
               <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" name="submit" class="btn btn-success"/>
              </div>
            </form>
            
          </div>
        </div>
      </div>
    </div>
    <!-- form tambah relawan -->
    </body>
    </html>
