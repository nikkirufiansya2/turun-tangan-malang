<body>
	
	<?php foreach ($kegiatan as $key): ?>
		

		<div class="card" style="margin:100px; background-color:#ebebeb;">
			<div class="card-body" style="text-align: center;">
				<h3>Masukan Nominal Donasi</h3>
				<form action="<?php echo base_url();?>index.php/donatur/Donatur/donasiUang" method="post">
					
					<input type="hidden" name="id_donatur" value="<?php echo $this->session->userdata("id_donatur");?>">
					<input type="hidden" name="id_kegiatan" value="<?php echo $key['id']?>">
					<input type="text" name="nominal" placeholder="Jumlah Donasi Anda" class="form-control">
					<select name="bank" class="form-control" style="margin-top: 10px;">
						<option>Pilih Bank</option>
						<option>BNI</option>
						<option>BRI</option>
						<option>BCA</option>
						<option>Mandiri</option>
					</select>
					<input type="submit" name="Donasi" class="btn btn-success" style="margin-top: 30px">	
				</form>
				
			</div>
		</div>

	<?php endforeach ?>
	<!-- Button trigger modal -->
	<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
		Launch demo modal
	</button> -->

	<!-- Modal -->
	<div class="modal fade" id="deskripsi" tabindex="-1" role="dialog" aria-labelledby="deskripsi" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-8">ID DONASI</div>
						<div class="col-lg-4">123</div>
					</div>
					<div class="row">
						<div class="col-lg-8">Nominal Donasi</div>
						<div class="col-lg-4">Rp.100000</div>
					</div>
					
					
					<div><b>Silahkan Transfer Ke Bank Terdekat dengan No Rek</b></div>
					<div class="row">
						<div class="col-lg-8">No Rek:</div>
						<div class="col-lg-4">2163876423844323</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<input type="submit" name="submit" class="btn btn-success"/>
				</div>
			</div>
		</div>
	</div>

	

	<!-- Footer -->
	<footer class="page-footer font-small bg-danger pt-4">

		<!-- Footer Links -->
		<div class="container text-center text-md-left">

			<!-- Footer links -->
			<div class="row text-center text-md-left mt-3 pb-3">

				<!-- Grid column -->
				<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
					<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Turun Tangan Malang</h6>
					<p style="color: white;">Gerakan Kecil Membangun Negeri</p>
				</div>
				<!-- Grid column -->

				<hr class="w-100 clearfix d-md-none">

				<!-- Grid column -->
				<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
					<h6 class="text-uppercase mb-4 font-weight-bold"></h6>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
				</div>
				<!-- Grid column -->

				<hr class="w-100 clearfix d-md-none">

				<!-- Grid column -->
				<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">

					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
				</div>

				<!-- Grid column -->
				<hr class="w-100 clearfix d-md-none">

				<!-- Grid column -->
				<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
					<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Contact</h6>
					<p style="color: white;">Facebook</p>
					<p style="color: white;">Twitter</p>
					<p style="color: white;">Instagram</p>
					<p style="color: white;">Line</p>
				</div>
				<!-- Grid column -->

			</div>
			<!-- Footer links -->

			<hr>

			<!-- Grid row -->
			<div class="row d-flex align-items-center">

				<!-- Grid column -->
				<div class="col-md-7 col-lg-8">

					<!--Copyright-->
					<p class="text-center text-md-left" style="color: white;">© 2020 Copyright:
						<a href="https://mdbootstrap.com/">
							<strong style="color: white;"> Turun Tangan Malang</strong>
						</a>
					</p>

				</div>

			</div>
			<!-- Grid row -->

		</div>
		<!-- Footer Links -->

	</footer>
	<!-- Footer -->

</body>
