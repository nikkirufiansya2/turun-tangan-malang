<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<body>
    <div id="login">
        <h3 class="text-center text-danger pt-5">Turun Tangan Malang</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="<?php echo base_url('index.php/dashboard/login'); ?>" method="post">
                            <h3 class="text-center text-danger">Login</h3>
                            <div class="form-group">
                                <label for="email" class="text-danger">Email:</label><br>
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-danger">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="remember-me" class="text-danger"><span></span> <span></span></label><br>
                                <input type="submit" name="submit" class="btn btn-danger btn-md" value="submit">
                            </div>
                            <div id="register-link" class="text-right">
                                <a href="<?php echo base_url(); ?>index.php/dashboard/formRegister" class="text-danger">Register here</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


<style type="text/css">
    body {
      margin: 0;
      padding: 0;
      background-color: #ddd;
      height: 100vh;
  }
  #login .container #login-row #login-column #login-box {
      margin-top: 120px;
      max-width: 600px;
      height: 320px;
      border: 1px solid #9C9C9C;
      background-color: #FFF;
  }
  #login .container #login-row #login-column #login-box #login-form {
      padding: 20px;
  }
  #login .container #login-row #login-column #login-box #login-form #register-link {
      margin-top: -85px;
  }
</style>