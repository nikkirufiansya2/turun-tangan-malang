<body>
	

<!--	<div class="container" style="margin-top: 10px;">-->
<!--		<div class="row">-->
<!--			<div class="col-lg-10">-->
<!--				<input type="text" name="" class="form-control" placeholder="Cari...">-->
<!--			</div>-->
<!--			<div class="col-lg-2">-->
<!--				<input type="submit" name="Cari" class="btn btn-danger">-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!---->
<!--	-->

	<div class="container">
		<!-- Page Heading -->
		<h1 class="my-4" style="text-align: center;">Kegiatan</h1>
		
		<div class="row">
			<?php foreach ($kegiatan as $key): ?>
				<div class="col-lg-4 col-sm-6 mb-4">
					<div class="card h-100">
						<a href="#"><img class="card-img-top" style="width: 100%; height: 200px;" src="<?php echo base_url()?>assets/foto/kegiatan/<?= $key->foto_kegiatan?>" alt=""></a>
						<div class="card-body">
							<h4 class="card-title"  style="text-align: center;">
								<a href="#"><?php echo $key->judul?></a>
							</h4>

							<i class="fa fa-money fa-1x">  Rp. <?php echo $key->laporan_dana?></i><br><br>
							<i class="fa fa-user fa-1x">  <?php echo $key->minimal_relawan?> Orang</i><br><br>
							<i class="fa fa-calendar">  <?php echo $key->tanggal?></i><br><br>
						</div>
						<a href="<?php echo base_url();?>index.php/dashboard/informasiKegiatan/<?= $key->id?>" style=" margin: 15px;" class="btn btn-danger">Lihat Detail</a>
					</div>
				</div>	
			<?php endforeach ?>
			
		</div>
		<!-- /.row -->

		<!-- Pagination -->
		<ul class="pagination justify-content-center">
			<li class="page-item">
				<a class="page-link" href="#" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="#">1</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="#">2</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="#">3</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="#" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
		</ul>
	</div>

	<!-- Footer -->
	<footer class="page-footer font-small bg-danger pt-4">

		<!-- Footer Links -->
		<div class="container text-center text-md-left">

			<!-- Footer links -->
			<div class="row text-center text-md-left mt-3 pb-3">

				<!-- Grid column -->
				<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
					<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Turun Tangan Malang</h6>
					<p style="color: white;">Gerakan Kecil Membangun Negeri</p>
				</div>
				<!-- Grid column -->

				<hr class="w-100 clearfix d-md-none">

				<!-- Grid column -->
				<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
					<h6 class="text-uppercase mb-4 font-weight-bold"></h6>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
				</div>
				<!-- Grid column -->

				<hr class="w-100 clearfix d-md-none">

				<!-- Grid column -->
				<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">

					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
					<p>
						<a href="#!"></a>
					</p>
				</div>

				<!-- Grid column -->
				<hr class="w-100 clearfix d-md-none">

				<!-- Grid column -->
				<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
					<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Contact</h6>
					<p style="color: white;">Facebook</p>
					<p style="color: white;">Twitter</p>
					<p style="color: white;">Instagram</p>
					<p style="color: white;">Line</p>
				</div>
				<!-- Grid column -->

			</div>
			<!-- Footer links -->

			<hr>

			<!-- Grid row -->
			<div class="row d-flex align-items-center">

				<!-- Grid column -->
				<div class="col-md-7 col-lg-8">

					<!--Copyright-->
					<p class="text-center text-md-left" style="color: white;">© 2020 Copyright:
						<a href="https://mdbootstrap.com/">
							<strong style="color: white;"> Turun Tangan Malang</strong>
						</a>
					</p>

				</div>

			</div>
			<!-- Grid row -->

		</div>
		<!-- Footer Links -->

	</footer>
	<!-- Footer -->

</body>
