<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Donatur</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard Donatur</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data Donatur</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<?php foreach ($donatur

		as $key): ?>

		<div class="card-body">
			<div class="row">
				<div class="col-lg-1">Nama :</div>
				<div class="col-lg-11"><?php echo $key['nama'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Alamat :</div>
				<div class="col-lg-11"><?php echo $key['alamat'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">No Hp :</div>
				<div class="col-lg-11"><?php echo $key['no_hp'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Email :</div>
				<div class="col-lg-11"><?php echo $key['email'] ?></div>
			</div>
		</div>
		<div class="card-footer">
			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#ubah-profile">Ubah Profil</a>
			<a href="" class="btn btn-dark" data-toggle="modal" data-target="#ubah-passowrd">Ubah Password</a>
		</div>
	</div>

	<!--form ubah password-->
	<div class="modal fade" id="ubah-passowrd" tabindex="-1" role="dialog" aria-labelledby="ubah-passowrd"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<label>Form Ubah Password</label>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="<?php echo base_url('index.php/donatur/Donatur/ubahPassword'); ?>" method="post"
						  enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
						<input type="hidden" name="id_users" value="<?php echo $key['id_users'] ?>">

						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Password</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="" name="password">
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<input type="submit" name="submit" class="btn btn-success"/>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>


	<!-- form edit profile -->
	<div class="modal fade" id="ubah-profile" tabindex="-1" role="dialog" aria-labelledby="ubah-profile"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<label>Form Ubah Profil</label>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" action="<?php echo base_url('index.php/donatur/Donatur/update'); ?>"
						  enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $this->session->userdata("id_users"); ?>">
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Nama</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="nama" id=""
									   value="<?php echo $key['nama'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Alamat</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="alamat"><?php echo $key['alamat'] ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">No Hp</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="" name="no_hp"
									   value="<?php echo $key['no_hp'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="" name="email"
									   value="<?php echo $key['email'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Foto</label>
							<div class="col-sm-10">
								<input type="hidden" name="foto_old" value="<?php echo $key['foto'] ?>">
								<input type="file" name="foto_donatur">
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<input type="submit" name="submit" class="btn btn-success"/>
						</div>
					</form>


				</div>
			</div>
		</div>
	</div>
	<!-- form edit profile -->

	<?php endforeach ?>

	<section class="content">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Data Laporan Keuangan</h3>

				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
							title="Collapse">
						<i class="fas fa-minus"></i></button>
				</div>
			</div>
			<div class="card-body">
				<table class="table table-bordered">
					<thead>
					<tr>
						<th>Nama Kegiatan</th>
						<th>Laporan Dana</th>
						<th>File Laporan</th>

					</tr>
					</thead>
					<tbody>
					<?php foreach ($laporan_keuangan as $key): ?>

						<tr>
							<td><?php echo $key->judul ?></td>
							<td><?php echo $key->laporan_dana ?></td>
							<td><?php
								if ($key->file_laporan != null){
									$link_address = base_url('index.php/donatur/Donatur/downloadFile/').$key->file_laporan;
									echo "<a href='".$link_address."' class='btn btn-success'>Download</a>";
								}
								?></td>
						</tr>
					<?php endforeach ?>

					</tbody>
				</table>
			</div>
			<div class="card-footer">
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Data Transaksi Uang</h3>

				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
							title="Collapse">
						<i class="fas fa-minus"></i></button>
				</div>
			</div>
			<div class="card-body">
				<table class="table table-bordered">
					<thead>
					<tr>
						<th>Tanggal Transaksi</th>
						<th>Jumlah Transaksi</th>
						<th>Bank</th>
						<th>Status</th>
						<th>Konfirmasi</th>
						<!--						<th>Laporan Data</th>-->
					</tr>
					</thead>
					<tbody>
					<?php foreach ($transaksi as $key): ?>

						<tr>
							<td><?php echo $key['waktu_donasi'] ?></td>
							<td><?php echo $key['jumlah_donasi'] ?></td>
							<td><?php echo $key['bank'] ?></td>
							<td><?php echo $key['status'] ?></td>
							<td><a href="" class="open-bukti-pembayaran btn btn-primary" data-toggle="modal"
								   data-id="<?php echo $key['id'] ?>" data-target="#upload-bukti">Upload Bukti</a></td>
							<!--							<td><a href="" class="btn btn-success">Download</a></td>-->
						</tr>
					<?php endforeach ?>

					</tbody>
				</table>
			</div>
			<div class="card-footer">
			</div>
		</div>



		<section class="content">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Data Transaksi Barang Metode Antar Barang</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
							<i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Waktu Donasi</th>
							<th>Deskripsi</th>
							<th>Jasa Kirim</th>
							<th>No Resi</th>
							<th>Status</th>

						</tr>
						</thead>
						<tbody>
						<?php foreach ($transaksi_barang as $key1): ?>

							<tr>
								<td><?php echo $key1['waktu_donasi'] ?></td>
								<td><?php echo $key1['deskripsi'] ?></td>
								<td><?php echo $key1['jasa_kirim'] ?></td>
								<td><?php echo $key1['no_resi'] ?></td>
								<td><?php echo $key1['status'] ?></td>
							</tr>
						<?php endforeach ?>

						</tbody>
					</table>
				</div>
				<div class="card-footer">
				</div>
			</div>

			<section class="content">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Data Transaksi Barang Metode Ambil Barang</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
									title="Collapse">
								<i class="fas fa-minus"></i></button>
						</div>
					</div>
					<div class="card-body">
						<table class="table table-bordered">
							<thead>
							<tr>
								<th>Nama Barang</th>
								<th>Alamat</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($transaksi_ambil_barang as $key1): ?>

								<tr>
									<td><?php echo $key1['nama_barang'] ?></td>
									<td><?php echo $key1['alamat'] ?></td>

								</tr>
							<?php endforeach ?>

							</tbody>
						</table>
					</div>
					<div class="card-footer">
					</div>
				</div>

			<!-- form upload bukti -->
			<div class="modal fade" id="upload-bukti" tabindex="-1" role="dialog" aria-labelledby="upload-bukti"
				 aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Upload Bukti Pembayaran</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form action="<?php echo base_url(); ?>index.php/donatur/Donatur/uploadBukti/" method="post"
							  enctype="multipart/form-data">
							<div class="modal-body">
								<input type="hidden" name="id" id="id" value="" class="idKegiatan">
								<div class="form-group">
									<input type="file" name="bukti_transfer">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<input type="submit" name="submit" id="submit" class="btn btn-primary">
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- form upload bukti -->

</body>
</html>
