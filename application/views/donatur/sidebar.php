  <div class="wrapper">
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="" class="brand-link">
        <span class="brand-text font-weight-light" style="margin-left: 10px;">Turun Tangan Malang</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
           <?php foreach ($donatur as $key): ?>
          <div class="image">
            <img src="<?= base_url(); ?>assets/foto/donatur/<?=$key['foto']?>" class="rounded-circle" alt="25x25" width="50px;" height="50pc" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">
                <?php echo $key['nama']?>  
              <?php endforeach ?>

            </a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
              <a href="" class="nav-link active">
               <i class="nav-icon fas fa-tachometer-alt"></i>
               <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/dashboard/'); ?>" class="nav-link informasi ">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Kembali Ke Halaman Utama
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('index.php/dashboard/logout'); ?>" class="nav-link informasi ">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Keluar
              </p>
            </a>
          </li>

        </nav>


        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
