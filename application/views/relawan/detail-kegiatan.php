<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Relawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard Relawan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Detail Kegiatan</h3>
        <?php foreach ($kegiatan as $key): ?>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">

           <div class="form-group">
            <label>Judul Kegiatan</label>
            <input type="text" name="" placeholder="<?php echo $key['judul']?>" readonly class="form-control">
          </div>

          <div class="form-group">
            <label>Status Kegiatan</label>
             <input type="text" name="" placeholder="<?php echo $key['status_kegiatan']?>" readonly class="form-control">
         </div>

         <div class="form-group">
          <label>Pesan Ajakan</label>
          <textarea class="form-control" readonly="" name="pesan_ajakan"><?php echo $key['pesan_ajakan']?></textarea>
        </div>

        <div class="form-group">
          <label>Deskripsi</label>
          <textarea class="form-control" readonly="" name="deskripsi"><?php echo $key['deksripsi']?></textarea>
        </div>

        <div class="form-group">
          <label>Minimal Relawan</label>
          <input type="text" name="min_relawan" readonly="" placeholder="<?php echo $key['minimal_relawan']?>" class="form-control">
        </div>

        <div class="form-group">
          <label>Minimal Donasi</label>
          <input type="text" name="min_donasi" readonly="" placeholder="<?php echo $key['minimal_donasi']?>" class="form-control">
        </div>

        <div class="form-group">
          <label>Tanggal Kegiatan</label>
          <input type="date" value="<?php echo $key['tanggal']?>" readonly="" name="tanggal_kegiatan" class="form-control">
        </div>

         <div class="form-group">
          <label>Laporan Dana</label>
          <input type="text" name="min_donasi" readonly="" placeholder="<?php echo $key['laporan_dana']?>" class="form-control">
        </div>
      <?php endforeach ?>
    </div>

  </div>



</body>
</html>
