<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Relawan</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard Relawan</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data Relawan</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<?php foreach ($relawan

		as $key): ?>


		<div class="card-body">
			<div class="row">
				<div class="col-lg-1">Nama :</div>
				<div class="col-lg-11"><?php echo $key['nama'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Divisi :</div>
				<div class="col-lg-11"><?php echo $key['divisi'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Profesi :</div>
				<div class="col-lg-11"><?php echo $key['profesi'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Fakultas :</div>
				<div class="col-lg-11"><?php echo $key['fakultas'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Jurusan :</div>
				<div class="col-lg-11"><?php echo $key['jurusan'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Alamat :</div>
				<div class="col-lg-11"><?php echo $key['alamat'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">No Hp :</div>
				<div class="col-lg-11"><?php echo $key['no_hp'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">ID Line :</div>
				<div class="col-lg-11"><?php echo $key['id_line'] ?></div>
			</div>
			<div class="row">
				<div class="col-lg-1">Email :</div>
				<div class="col-lg-11"><?php echo $key['email'] ?></div>
			</div>
		</div>
		<div class="card-footer">
			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#upload-bukti">Ubah Profil</a>
			<a href="" class="btn btn-dark" data-toggle="modal" data-target="#ubah-passowrd">Ubah Password</a>
		</div>
	</div>

	<!--form ubah password-->
	<div class="modal fade" id="ubah-passowrd" tabindex="-1" role="dialog" aria-labelledby="ubah-passowrd"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<label>Form Ubah Password</label>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="<?php echo base_url('index.php/relawan/Relawan/ubahPassword'); ?>" method="post"
						  enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
						<input type="hidden" name="id_users" value="<?php echo $key['id_users'] ?>">

						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Password</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="" name="password">
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<input type="submit" name="submit" class="btn btn-success"/>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<!-- form edit profile -->
	<div class="modal fade" id="upload-bukti" tabindex="-1" role="dialog" aria-labelledby="upload-bukti"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<label>Form Ubah Profil</label>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>

				</div>
				<div class="modal-body">
					<form action="<?php echo base_url('index.php/relawan/Relawan/ubahRelawan'); ?>" method="post"
						  enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
						<input type="hidden" name="id_users" value="<?php echo $key['id_users'] ?>">
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Nama</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="" name="nama"
									   value="<?php echo $key['nama'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Divisi</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="divisi"
									   value="<?php echo $key['divisi'] ?>" id="">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Profesi</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="" name="profesi"
									   value="<?php echo $key['profesi'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Fakultas</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="fakultas"
									   value="<?php echo $key['fakultas'] ?>" id="">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Jurusan</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="jurusan"
									   value="<?php echo $key['jurusan'] ?>" id="">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Alamat</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="alamat"><?php echo $key['alamat'] ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">No Hp</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="no_hp" value="<?php echo $key['no_hp'] ?>"
									   id="">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">ID Line</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="id_line"
									   value="<?php echo $key['id_line'] ?>" id="">
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Email</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" name="email"
									   value="<?php echo $key['email'] ?>" id="">
							</div>
						</div>

						<div class="form-group row">
							<label for="" class="col-sm-2 col-form-label">Foto</label>
							<div class="col-sm-10">
								<input type="hidden" name="foto_old" value="<?php echo $key['foto'] ?>">
								<input type="file" name="foto_relawan" value="">
								<img src="<?= base_url() ?>assets/foto/relawan/<?= $key['foto'] ?>" alt=""
									 style="width: 100px; height: 100px;" srcset="">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<input type="submit" name="submit" class="btn btn-success"/>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
	<?php endforeach ?>
	<!-- form edit profile -->


	<section class="content">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Kegiatan</h3>

				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
							title="Collapse">
						<i class="fas fa-minus"></i></button>
				</div>
			</div>
			<div class="card-body">
				<table class="table table-bordered">
					<thead>
					<tr>
						<th>Kegiatan</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($kegiatan as $key): ?>
						<tr>
							<td><?php echo $key->judul ?></td>
							<td><?php echo $key->status_kegiatan ?></td>
							<td>
								<a href="<?php echo base_url(); ?>index.php/relawan/Relawan/detailKegiatan/<?php echo $key->id ?>"
								   class="btn btn-primary">Lihat Detail</a>
								<a href="<?php echo base_url(); ?>index.php/relawan/Relawan/pemberitahuan/<?php echo $key->id ?>"
								   class="btn btn-success">Lihat Pemberitahuan</a>
							</td>
						</tr>
					<?php endforeach ?>

					</tbody>
				</table>
			</div>
			<div class="card-footer">
			</div>
		</div>


		<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<label>Detail Kegiatan</label>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-2"><b>Kegiatan</b></div>
							<div class="col-lg-10"></div>
						</div>
						<div class="row">
							<div class="col-lg-2"><b>Deskripsi</b></div>
							<div class="col-lg-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
								consectetur pellentesque consequat. Duis in felis posuere, hendrerit urna vehicula,
								convallis dui. Suspendisse tincidunt venenatis ante a venenatis. Pellentesque habitant
								morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris in
								condimentum enim. Quisque a nunc a purus imperdiet suscipit. Donec rutrum tincidunt
								dapibus.
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2"><b>Tanggal </b></div>
							<div class="col-lg-10">10/12/2020</div>
						</div>
						<div class="row">
							<div class="col-lg-2"><b>Alamat</b></div>
							<div class="col-lg-10">Jl. Lorem ipsum dolor sit amet</div>
						</div>
						<div class="row">
							<div class="col-lg-2"><b>Foto </b></div>
							<div class="col-lg-10"><img src="http://placehold.it/100x100"
														style="width: 100px; height: 100px;"></div>
						</div>
						<div class="row">
							<div class="col-lg-2"><b>Dana</b></div>
							<div class="col-lg-10">Rp.1000000</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="pemberitahuan" tabindex="-1" role="dialog" aria-labelledby="pemberitahuan"
			 aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<label>Pemberitahuan</label>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<!-- pemberitahuan -->
						<div class="container">
							<div class="card card-primary collapsed-card">
								<div class="card-header">
									<h3 class="card-title">12/10/2020</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
													class="fas fa-plus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									Pesan Pemberitahuan
								</div>
							</div>
						</div>

						<div class="container">
							<div class="card card-primary collapsed-card">
								<div class="card-header">
									<h3 class="card-title">12/10/2020</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
													class="fas fa-plus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									Pesan Pemberitahuan
								</div>
							</div>
						</div>

						<div class="container">
							<div class="card card-primary collapsed-card">
								<div class="card-header">
									<h3 class="card-title">12/10/2020</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
													class="fas fa-plus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									Pesan Pemberitahuan
								</div>
							</div>
						</div>

						<div class="container">
							<div class="card card-primary collapsed-card">
								<div class="card-header">
									<h3 class="card-title">12/10/2020</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
													class="fas fa-plus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									Pesan Pemberitahuan
								</div>
							</div>
						</div>
						<!-- pemberitahuan -->
					</div>
				</div>
			</div>
		</div>

</body>
</html>
