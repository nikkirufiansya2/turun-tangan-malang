<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Relawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard Relawan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card" style="margin-bottom: 500px;">
      <div class="card-header">
        <h3 class="card-title">Detail Kegiatan</h3>
        <br>
        <br>
        <div class="container">
         <?php foreach ($pemberitahuan as $key): ?>
          <div class="card card-primary collapsed-card">
            <div class="card-header">
              <h3 class="card-title"><?php echo $key['tanggal']?></h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <?php echo $key['pesan']?>
            </div>
          </div>
          <?php endforeach ?>
        </div>
    </div>

  </div>
</section>



</body>
</html>
