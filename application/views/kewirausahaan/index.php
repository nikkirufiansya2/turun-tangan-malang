<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Ketua Divis Kewirausahaan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard Kewirausahaan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Kegiatan</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">

         <div class="row">
          <div class="container-fluid">
            <div class="card card-primary card-outline card-tabs">
              <div class="card-header p-0 pt-1 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Data Donasi Keuangan</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Data Donasi Barang</a>
                  </li>
                  
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <label>Data Donasi Keuangan</label>
                    <table id="example2" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Nama Donatur</th>
                          <th>Kegiatan</th>
                          <th>Nominal Donasi</th>
                          <th>Tanggal Donasi</th>
                          <th>Bukti Transfer</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($transaksi as $key): ?>

                          <tr>
                            <td><?php echo $key->nama?></td>
                            <td><?php echo $key->judul?></td>
                            <td><?php echo $key->jumlah_donasi?></td>
                            <td><?php echo $key->waktu_donasi?></td>
                            <td>
                              <?php 

                              if ($key->bukti_transfer == null) {
                                echo "Bukti Transfer Tidak Ada";
                              }else{
                                ?>
                                <img src="<?php echo base_url();?>assets/foto/bukti/<?php echo $key->bukti_transfer?>" height="150px" width="150px">

                              <?php } ?>

                            </td>
                            <td><?php echo $key->status?></td>
                            <td>
                              <a href=""></a>
                              <a href="<?php echo base_url();?>index.php/kewirausahaan/Kewirausahaan/verifikasi/<?php echo $key->id?>" class="btn btn-success">Verifikasi</a> 
                              <a href="<?php echo base_url()?>index.php/kewirausahaan/Kewirausahaan/delete/<?php echo $key->id?>" class="btn btn-danger">Hapus</a>
                            </td>
                          </tr>
                        <?php endforeach ?>
                        s
                      </tfoot>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                     <label>Data Donasi Barang</label>
                    <table id="example2" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Nama Donatur</th>
                          <th>Kegiatan</th>
                          <th>Tanggal Donasi</th>
                          <th>Deskripsi</th>
                          <th>Jasa Kirim</th>
                          <th>No RESI</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($transaksi_barang as $key1): ?>

                          <tr>
                            <td><?php echo $key1->nama?></td>
                            <td><?php echo $key1->judul?></td>
                            <td><?php echo $key1->waktu_donasi?></td>
                            <td><?php echo $key1->deskripsi?></td>
                            <td><?php echo $key1->jasa_kirim?></td>
                            <td><?php echo $key1->no_resi?></td>
                            <td><?php echo $key1->status?></td>
                            <td>
                              <a href=""></a>
                              <a href="<?php echo base_url();?>index.php/kewirausahaan/Kewirausahaan/verifikasiBarang/<?php echo $key1->id?>" class="btn btn-success">Verifikasi</a> 
                              <a href="<?php echo base_url()?>index.php/kewirausahaan/Kewirausahaan/deleteTransaksiBarang/<?php echo $key1->id?>" class="btn btn-danger">Hapus</a>
                            </td>
                          </tr>
                        <?php endforeach ?>
                        s
                      </tfoot>
                    </table>
                  </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail Donasi</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-2">Nama</div>
                <div class="col-lg-10">Alexander</div>
              </div>
              <div class="row">
                <div class="col-lg-2">Kegiatan</div>
                <div class="col-lg-10">Ruang Belajar</div>
              </div>
              <div class="row">
                <div class="col-lg-2">Nominal</div>
                <div class="col-lg-10">Rp.000</div>
              </div>
              <div class="row">
                <div class="col-lg-2">Tanggal</div>
                <div class="col-lg-10">2020-11-12</div>
              </div>
              <div class="row">
                <div class="col-lg-2">Status</div>
                <div class="col-lg-10">Lewat Batas Waktu</div>
              </div>
              <div class="row">
                <div class="col-lg-2">Struk</div>
                <div class="col-lg-10"><img class="card-img-top" src="http://placehold.it/100x100" alt=""></div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
      </div>




    </body>
    </html>
