<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Ketua PPG</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Tambah Kegiatan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tambah Kegiatan</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label>Judul Kegiatan</label>
            <input type="text" name="nama_kegiatan" class="form-control">
          </div>

          <div class="form-group">
            <label>Status Kegiatan</label>
            <select class="form-control">
              <option>Promosi Kegiatan</option>
              <option>Test 1</option>
              <option>Test 2</option>
            </select>
          </div>

          <div class="form-group">
            <label>Pesan Ajakan</label>
            <textarea class="form-control" name="pesan_ajakan"></textarea>
          </div>

          <div class="form-group">
            <label>Deskripsi</label>
            <textarea class="form-control" name="deskripsi_kegiatan"></textarea>
          </div>

          <div class="form-group">
            <label>Minimal Relawan</label>
            <input type="text" name="min_relawan" class="form-control">
          </div>

          <div class="form-group">
            <label>Minimal Donasi</label>
            <input type="text" name="min_donasi" class="form-control">
          </div>

          <div class="form-group">
            <label>Tanggal Kegiatan</label>
            <input type="date" name="tanggal_kegiatan" class="form-control">
          </div>

          <label>Gambar Kegiatan</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>

          <div class="form-group">
            <label>Laporan Dana</label>
            <input type="text" name="laporan_data" class="form-control">
          </div>  

          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-success">
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    CKEDITOR.replace( 'deskripsi_kegiatan');
  </script>
</body>
</html>
