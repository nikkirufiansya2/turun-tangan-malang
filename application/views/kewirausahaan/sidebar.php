<div class="wrapper">
	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<!-- Brand Logo -->
		<a href="" class="brand-link">
			<span class="brand-text font-weight-light" style="margin-left: 10px;">Turun Tangan Malang</span>
		</a>

		<!-- Sidebar -->
		<div class="sidebar">
			<!-- Sidebar user panel (optional) -->
			<div class="user-panel mt-3 pb-3 mb-3 d-flex">
				<div class="image">
					<img src="<?php echo base_url(); ?>assets/adminlte/dist/img/user2-160x160.jpg"
						 class="img-circle elevation-2" alt="User Image">
				</div>
				<div class="info">
					<a href="#" class="d-block"><?php echo $this->session->userdata("nama"); ?></a>
				</div>
			</div>

			<!-- Sidebar Menu -->
			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
					data-accordion="false">

					<li class="nav-item">
						<a href="<?php echo base_url(); ?>index.php/kewirausahaan/Kewirausahaan/" class="nav-link">
							<i class="nav-icon fas fa-heart"></i>
							<ion-icon name="heart-outline"></ion-icon>
							<p>
								Kelola Donasi
							</p>
						</a>
					</li>

					<li class="nav-item">
						<a href="<?php echo base_url(); ?>index.php/kewirausahaan/Kewirausahaan/kelolaDonatur"
						   class="nav-link">
							<i class="nav-icon fas fa-users"></i>
							<p>
								Kelola Donatur
							</p>
						</a>
					</li>

					<li class="nav-item">
						<a href="<?php echo base_url(); ?>index.php/kewirausahaan/Kewirausahaan/profile/<?php echo $this->session->userdata("id_users"); ?>"
						   class="nav-link">
							<i class="nav-icon fas fa-user"></i>
							<p>
								Kelola Profil
							</p>
						</a>
					</li>

					<li class="nav-item">
						<a href="<?php echo base_url('index.php/dashboard/logout'); ?>" class="nav-link informasi ">
							<i class="nav-icon fas fa-sign-out-alt"></i>
							<p>
								Keluar
							</p>
						</a>
					</li>
			</nav>
			<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
