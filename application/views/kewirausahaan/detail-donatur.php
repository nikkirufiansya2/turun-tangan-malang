<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Kewiraushaan</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard Kewirausahaan</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data Profile Donatur</h3>
			<?php foreach ($users as $key): ?>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<form action="<?php echo base_url('index.php/kewirausahaan/Kewirausahaan/prosesUpdateUsers'); ?>"
				  method="post"
				  enctype="multipart/form-data">
				<div class="form-group">
					<label>Name</label>
					<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
					<input type="text" readonly name="name" value="<?php echo $key['nama'] ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="text" readonly name="email" value="<?php echo $key['email'] ?>" class="form-control">
				</div>
				
				<div class="form-group">
					<label>Alamat</label>
					<input type="text" readonly name="email" value="<?php echo $key['alamat'] ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Nomor Telepone</label>
					<input type="text" readonly name="email" value="<?php echo $key['no_hp'] ?>" class="form-control">
				</div>

				<div class="form-group">
					<label>Foto Profile</label><b>
				

				</div>
					<img src="<?php echo base_url('/assets/foto/donatur/');?><?php echo $key['foto']?>"  height="150px" width="150px">
				<!-- <div class="form-group">
					<input type="submit" name="submit" class="btn btn-success"/>
				</div> -->

				<?php endforeach ?>
			</form>
		</div>

	</div>
	</div>
</section>

</body>
</html>
