<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Ketua Divis Kewirausahaan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard Kewirausahaan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Donatur</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>

              <tr>
                <th>Email</th>
                <th>Nama</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($donatur as $key ): ?>
                <tr>
                  <td><?php echo $key->email?></td>
                  <td><?php echo $key->nama?></td>
                  <td>
                    <a href="<?php echo base_url();?>index.php/kewirausahaan/Kewirausahaan/detailDonatur/<?php echo $key->id?>" class="btn btn-primary">Detail</a> 
                  </td>
                </tr>

              </tfoot>
            <?php endforeach ?>
          </table>
        </div>


        <!-- Modal -->
        

      </body>
      </html>
