<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Ketua PPG</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard PPG</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data Kegiatan</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">

			<a href="<?php echo base_url(); ?>index.php/ppg/Ppg/tambahKegiatan" class="btn btn-primary">Tambah
				Kegiatan</a><br>
			<br>
			<?php foreach ($kegiatan as $key): ?>
				<div class="card card-primary collapsed-card">
					<div class="card-header">
						<h3 class="card-title"><?php echo $key->judul ?> <span
									class="btn btn-danger"><?php echo $key->status_kegiatan ?></span></h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
										class="fas fa-plus"></i>
							</button>
						</div>
					</div>
					<div class="card-body">
						<label><?php echo $key->judul ?></label>
						<p><?php echo $key->deksripsi ?></p>
<!--						<a href="--><?php //echo base_url(); ?><!--index.php/ppg/Ppg/updateKegiatan/--><?php //echo $key->id ?><!--"-->
<!--						   class="btn btn-warning">Edit</a>-->

						<a href="<?php echo base_url(); ?>index.php/ppg/Ppg/updateKegiatan/<?php echo $key->id ?>"
						   class="btn btn-warning">Edit</a>


						<a href="<?php echo base_url(); ?>index.php/ppg/Ppg/delete/<?= $key->id ?>"
						   class="btn btn-danger">Hapus</a><br>
						<hr>
						<button class="open-kahadiran btn btn-primary" data-toggle="modal"
								data-id="<?php echo $key->id ?>" data-target="#kehadiran">Kehadiran
						</button>
						<button class="open-laporan-dana btn btn-secondary" data-id="<?php echo $key->id ?>"
								data-target="#upload-laporan-dana" data-toggle="modal">Laporan Dana
						</button>
						<button class="open-dokumentasi btn btn-success" data-toggle="modal"
								data-id="<?php echo $key->id ?>" data-target="#upload-dokumentasi">Upload Dokumentasi
						</button>
						<a href="<?php echo base_url(); ?>index.php/ppg/Ppg/komentarKegiatan/<?php echo $key->id ?>"
						   class="btn btn-info">Komentar</a>
						<button data-toggle="modal" data-target="#update-pemberitahuan" data-id="<?php echo $key->id ?>"
								class="open-pemberitahuan btn btn-dark">Pemberitahuan
						</button>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>

	<!-- laporan Kehadiran -->
	<div class="modal fade" id="kehadiran" tabindex="-1" role="dialog" aria-labelledby="kehadiran" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Daftar Relawan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="<?php echo base_url(); ?>index.php/ppg/Ppg/kehadiranRelawan/" method="post">
					<div class="modal-body">
						<div class="form-group">
							<input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Pilih Relawan</label>
								<select name="relawan" class="form-control" id="exampleFormControlSelect1">
									<?php foreach ($relawan as $key): ?>
										<option value="<?php echo $key->id ?>"><?php echo $key->nama ?></option>
									<?php endforeach ?>
								</select>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<input type="submit" name="submit" id="submit" class="btn btn-primary">
					</div>
				</form>

			</div>
		</div>
	</div>

	<!-- laporan dana -->
	<div class="modal fade" id="upload-laporan-dana" tabindex="-1" role="dialog" aria-labelledby="upload-laporan-dana"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Update Laporan Dana</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="<?php echo base_url(); ?>index.php/ppg/Ppg/updateLaporanDana/" enctype="multipart/form-data" method="post">
					<div class="modal-body">
						<div class="form-group">
							<label for="">Total Dana Kegiatan</label>
							<input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">
							<input type="text" name="laporan_dana" class="form-control" placeholder="Rp.000" required>
						</div>
						<div class="form-group">
							<label for="">Rincian File Keuangan (xls/xlsx)</label>
							<input type="file" name="file_laporan" class="form-control" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<input type="submit" name="submit" id="submit" class="btn btn-primary">
					</div>
				</form>

			</div>
		</div>
	</div>

	<!-- laporan dokumentasi -->
	<div class="modal fade" id="upload-dokumentasi" tabindex="-1" role="dialog" aria-labelledby="upload-dokumentasi"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Upload Dokumentasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="<?php echo base_url(); ?>index.php/ppg/Ppg/uploadDokumentasi/" method="post"
					  enctype="multipart/form-data">
					<div class="modal-body">
						<input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">
						<div class="form-group">
							<input type="file" name="foto_kegiatan">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<input type="submit" name="submit" id="submit" class="btn btn-primary">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Pemberitahuan -->
	<div class="modal fade" id="update-pemberitahuan" tabindex="-1" role="dialog" aria-labelledby="update-pemberitahuan"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Update Pemberitahuan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="<?php echo base_url(); ?>index.php/ppg/Ppg/uploadPemberitahuan/" method="post">
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">

								<textarea name="pemberitahuan" class="form-control" placeholder="Pesan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<input type="submit" name="submit" id="submit" class="btn btn-primary">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<!--update-kegiatan-->
	<div class="modal fade" id="update-kegiatan" tabindex="-1" role="dialog" aria-labelledby="update-kegiatan"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<label>Form Update Kegiatan</label>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>

				</div>
				<?php foreach ($kegiatan as $key): ?>
					<div class="modal-body">
						<form>

							<div class="card-body">
								<input type="text" name="id" value="<?= $key->id ?>">
								<div class="form-group">
									<label>Judul Kegiatan</label>
									<input type="text" name="nama_kegiatan" value="<?= $key->judul ?>"
										   class="form-control">
								</div>

								<div class="form-group">
									<label>Status Kegiatan</label>
									<select class="form-control" name="status_kegiatan">
										<?php foreach ($kegiatan as $key): ?>
											<option><?php echo $key->status_kegiatan ?></option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label>Pesan Ajakan</label>
									<textarea class="form-control"
											  name="pesan_ajakan"><?= $key->pesan_ajakan ?></textarea>
								</div>

								<div class="form-group">
									<label>Deskripsi</label>
									<textarea class="form-control"
											  name="deskripsi_kegiatan"><?= $key->deksripsi ?></textarea>
								</div>

								<div class="form-group">
									<label>Minimal Relawan</label>
									<input type="text" name="min_relawan" value="<?= $key->minimal_relawan ?>"
										   class="form-control">
								</div>

								<div class="form-group">
									<label>Minimal Donasi</label>
									<input type="text" name="min_donasi" value="<?= $key->minimal_donasi ?>"
										   class="form-control">
								</div>

								<div class="form-group">
									<label>Tanggal Kegiatan</label>
									<input type="date" name="tanggal_kegiatan" value="<?= $key->tanggal ?>"
										   class="form-control">
								</div>

								<label>Gambar Kegiatan</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="customFile">
									<label class="custom-file-label" for="customFile">Choose file</label>
								</div>
								<img src="<?php echo base_url('assets/foto/kegiatan/$key->foto_kegiatan') ?>" alt="">

								<div class="form-group">
									<label>Laporan Dana</label>
									<input type="text" name="laporan_dana" class="form-control">
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<input type="submit" name="submit" class="btn btn-success"/>
								</div>

						</form>

					</div>
				<?php endforeach ?>

			</div>
		</div>
	</div>

</body>
</html>
<script type="text/javascript">


</script>
