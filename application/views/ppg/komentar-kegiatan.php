<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard Ketua PPG</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Dashboard PPG</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Komentar Kegiatan</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th>Komentar</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($komentar as $key): ?>
                <tr>
                  <td><?php echo $key->isi_komentar?></td>
                  <td><a href="<?php echo base_url();?>index.php/ppg/Ppg/deleteKomentar/<?php echo $key->id?>" class="btn btn-danger">Delete</a></td>
                </tr>  
              <?php endforeach ?>
              
            </tbody>
          </table>
        </div>
      </div>
      <!-- laporan Kehadiran -->
      <div class="modal fade" id="kehadiran" tabindex="-1" role="dialog" aria-labelledby="kehadiran" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Kehadiran</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="container">
              <div class="form-group">
                <label>Relawan</label>
                <div class="select2-purple">
                  <select class="select2 form-control" multiple="multiple" data-placeholder="Pilih Relawan" data-dropdown-css-class="select2-purple">
                    <?php foreach ($relawan as $data): ?>  
                      <option><?php echo $data->nama?></option>
                    <?php endforeach ?>

                  </select>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <!-- laporan dana -->
      <div class="modal fade" id="upload-laporan-dana" tabindex="-1" role="dialog" aria-labelledby="upload-laporan-dana" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Update Laporan Dana</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url(); ?>index.php/ppg/Ppg/updateLaporanDana/" method="post">
              <div class="modal-body">
                <div class="form-group">
                  <input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">
                  <input type="text" name="laporan_dana" class="form-control" placeholder="Rp.000">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" name="submit" id="submit" class="btn btn-primary" >
              </div>
            </form>

          </div>
        </div>
      </div>

      <!-- laporan dokumentasi -->
      <div class="modal fade" id="upload-dokumentasi" tabindex="-1" role="dialog" aria-labelledby="upload-dokumentasi" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Upload Dokumentasi</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url(); ?>index.php/ppg/Ppg/uploadDokumentasi/" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">
                <div class="form-group">
                 <input type="file" name="foto_kegiatan">
               </div>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <input type="submit" name="submit" id="submit" class="btn btn-primary" >
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Pemberitahuan -->
    <div class="modal fade" id="update-pemberitahuan" tabindex="-1" role="dialog" aria-labelledby="update-pemberitahuan" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Update Pemberitahuan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <form action="<?php echo base_url(); ?>index.php/ppg/Ppg/uploadPemberitahuan/" method="post">
            <div class="modal-body">
              <div class="form-group">
                <input type="hidden" name="id_kegiatan" id="idKegiatan" value="" class="idKegiatan">

                <textarea name="pemberitahuan" class="form-control" placeholder="Pesan"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <input type="submit" name="submit" id="submit" class="btn btn-primary" >
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>




  <div class="modal fade" id="update-kegiatan" tabindex="-1" role="dialog" aria-labelledby="update-kegiatan" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <label>Form Update Kegiatan</label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="modal-body">
          <form>
           <div class="card-body">
            <div class="form-group">
              <label>Judul Kegiatan</label>
              <input type="text" name="nama_kegiatan" class="form-control">
            </div>

            <div class="form-group">
              <label>Status Kegiatan</label>
              <select class="form-control">
                <option>Promosi Kegiatan</option>
                <option>Test 1</option>
                <option>Test 2</option>
              </select>
            </div>

            <div class="form-group">
              <label>Pesan Ajakan</label>
              <textarea class="form-control" name="pesan_ajakan"></textarea>
            </div>

            <div class="form-group">
              <label>Deskripsi</label>
              <textarea class="form-control" name="deskripsi_kegiatan"></textarea>
            </div>

            <div class="form-group">
              <label>Minimal Relawan</label>
              <input type="text" name="min_relawan" class="form-control">
            </div>

            <div class="form-group">
              <label>Minimal Donasi</label>
              <input type="text" name="min_donasi" class="form-control">
            </div>

            <div class="form-group">
              <label>Tanggal Kegiatan</label>
              <input type="date" name="tanggal_kegiatan" class="form-control">
            </div>

            <label>Gambar Kegiatan</label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Choose file</label>
            </div>

            <div class="form-group">
              <label>Laporan Dana</label>
              <input type="text" name="laporan_dana" class="form-control">
            </div>  
          </form>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" name="submit" class="btn btn-success"/>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>
</html>
