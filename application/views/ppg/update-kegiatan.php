<!DOCTYPE html>
<html>
<body class="hold-transition sidebar-mini layout-fixed">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Dashboard Ketua PPG</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Dashboard Ketua PPG</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
		<div class="card-header">
			<h3 class="card-title">Detail Kegiatan</h3>
			<?php foreach ($kegiatan as $key): ?>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<form action="<?php echo base_url('index.php/ppg/Ppg/editKegiatan'); ?>" method="post"
			  enctype="multipart/form-data">
			<div class="card-body">
				<div class="form-group">
					<label>Judul Kegiatan</label>
					<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
					<input type="text" name="nama_kegiatan" value="<?php echo $key['judul'] ?>" class="form-control">
				</div>

				<div class="form-group">
					<label>Status Kegiatan</label>
					<input type="text" name="status_kegiatan" value="<?php echo $key['status_kegiatan'] ?>"
						   class="form-control">
				</div>

				<div class="form-group">
					<label>Pesan Ajakan</label>
					<textarea class="form-control" name="pesan_ajakan"><?php echo $key['pesan_ajakan'] ?></textarea>
				</div>

				<div class="form-group">
					<label>Deskripsi</label>
					<textarea class="form-control" name="deskripsi"><?php echo $key['deksripsi'] ?></textarea>
				</div>

				<div class="form-group">
					<label>Alamat</label>
					<textarea class="form-control" name="alamat"><?php echo $key['alamat'] ?></textarea>
				</div>

				<div class="form-group">
					<label>Minimal Relawan</label>
					<input type="text" name="min_relawan" value="<?php echo $key['minimal_relawan'] ?>"
						   class="form-control">
				</div>

				<div class="form-group">
					<label>Minimal Donasi</label>
					<input type="text" name="min_donasi" value="<?php echo $key['minimal_donasi'] ?>"
						   class="form-control">
				</div>

				<div class="form-group">
					<label>Tanggal Kegiatan</label>
					<input type="date" value="<?php echo $key['tanggal'] ?>" name="tanggal_kegiatan"
						   class="form-control">
				</div>

				<div class="form-group">
					<label>Laporan Dana</label>
					<input type="text" name="min_donasi" value="<?php echo $key['laporan_dana'] ?>"
						   class="form-control">
				</div>

				<label>Gambar Kegiatan</label>
				<div class="custom-file">
					<input type="file" name="foto_kegiatan">
					<input type="hidden" name="foto_old" value="<?= $key['foto_kegiatan']?>">
				</div>
				<div>
					<img src="<?= base_url() ?>assets/foto/kegiatan/<?= $key['foto_kegiatan'] ?>"
						 style="width: 500px; height: 500px;" alt="" srcset="">
				</div>
				<br>
				<div class="form-group">
					<input type="submit" name="submit" class="btn btn-success">
				</div>
			</div/>
				<?php endforeach ?>
		</form>
		</div>

		</div>

</section>


</body>
</html>
