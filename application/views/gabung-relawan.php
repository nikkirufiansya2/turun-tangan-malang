<body>

<div class="container">
	<?php foreach ($kegiatan as $key): ?>
	<div class="card">
		<div class="card-body">
			Apakah Anda ingin bergabung menjadi relawan pada kegiatan ini
			<div class="container" style="margin-top: 20px;">
				<center>
					<form action="<?php echo base_url();?>index.php/donatur/Donatur/prosesGabungRelawan" method="post">
						<input type="hidden" name="id_donatur" value="<?php echo $this->session->userdata("id_donatur");?>">
						<input type="hidden" name="id_kegiatan" value="<?php echo $key['id']?>">

						<a href="<?php echo base_url('/index.php')?>" class="btn btn-danger">Tidak</a>
						<input type="submit" value="Ya"class="btn btn-primary">
					</form>

				</center>
			</div>
		</div>
	</div>
	<?php endforeach;?>
</div>


<!-- Footer -->
<footer class="page-footer font-small bg-danger pt-4">

	<!-- Footer Links -->
	<div class="container text-center text-md-left">

		<!-- Footer links -->
		<div class="row text-center text-md-left mt-3 pb-3">

			<!-- Grid column -->
			<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
				<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Turun Tangan Malang</h6>
				<p style="color: white;">Gerakan Kecil Membangun Negeri</p>
			</div>
			<!-- Grid column -->

			<hr class="w-100 clearfix d-md-none">

			<!-- Grid column -->
			<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
				<h6 class="text-uppercase mb-4 font-weight-bold"></h6>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
			</div>
			<!-- Grid column -->

			<hr class="w-100 clearfix d-md-none">

			<!-- Grid column -->
			<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">

				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
			</div>

			<!-- Grid column -->
			<hr class="w-100 clearfix d-md-none">

			<!-- Grid column -->
			<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
				<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Contact</h6>
				<p style="color: white;">Facebook</p>
				<p style="color: white;">Twitter</p>
				<p style="color: white;">Instagram</p>
				<p style="color: white;">Line</p>
			</div>
			<!-- Grid column -->

		</div>
		<!-- Footer links -->

		<hr>

		<!-- Grid row -->
		<div class="row d-flex align-items-center">

			<!-- Grid column -->
			<div class="col-md-7 col-lg-8">

				<!--Copyright-->
				<p class="text-center text-md-left" style="color: white;">© 2020 Copyright:
					<a href="https://mdbootstrap.com/">
						<strong style="color: white;"> Turun Tangan Malang</strong>
					</a>
				</p>

			</div>

		</div>
		<!-- Grid row -->

	</div>
	<!-- Footer Links -->

</footer>
<!-- Footer -->

</body>
