<body>

<?php foreach ($kegiatan

			   as $key): ?>
	<div>
		<img src="<?php echo base_url() ?>assets/foto/kegiatan/<?= $key['foto_kegiatan'] ?>" style="max-height: 550px;"
			 class="d-block w-100">
	</div>


<div class="container">

	<div class="row">
		<div class="col-8">
			<h3><?php echo $key['judul'] ?></h3>
			<p><?php echo $key['deksripsi'] ?></p>
		</div>

		<div class="col-4">
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<h5 class="card-title">Dana Relawan yang terkumpul saat ini</h5>
					<i class="fa fa-money fa-1x" style="margin-bottom: 10px;">
						Rp. <?php echo $key['laporan_dana'] ?></i><br>
					<i class="fa fa-user fa-1x" style="margin-bottom: 10px;"> <?php echo $key['minimal_relawan'] ?>
						orang</i><br>
					<i class="fa fa-calendar"> <?php echo $key['tanggal'] ?></i><br>
					<i class="fa fa-map-marker"></i> <?php echo $key['alamat'] ?><br>
					<?php
					if ($this->session->userdata("status") == "login") {
						?>
						<center style="margin-top: 10px;">
							<a href="<?php echo base_url() ?>index.php/dashboard/metodeDonasiBarang/<?= $key['id'] ?>"
							   class="btn btn-secondary">Barang</a>
							<a href="<?php echo base_url() ?>index.php/dashboard/donasiUang/<?= $key['id'] ?>"
							   class="btn btn-secondary">Uang</a><br><br>

							<?php
								foreach ($gabung as $g):
								if (empty($kegiatan)){
									echo "tombol ada";
								}else{
									echo "tombol hilang";
								}
								endforeach;;
							?>
							<a href="<?php echo base_url() ?>index.php/dashboard/gabungRelawan/<?= $key['id'] ?>"
							   class="btn btn-warning">Bergabung</a><br><br>
						</center>
						<?php
					} else {

					}
					?>
					<center>

					</center>
				</div>
			</div>
			<?php endforeach ?>

			<div class="card" style="width: 18rem; margin-top: 10px;">
				<div class="card-body">
					<h5 class="card-title">Donator yang telah bergabung</h5>
					<?php foreach ($donatur as $donatur): ?>
						<div class="row">
							<div class="col-sm-4">
								<img class="rounded-circle" alt="25x25" width="50px;" height="50pc"
									 src="<?php echo base_url(); ?>assets/foto/donatur/<?php echo $donatur->foto ?>"
									 data-holder-rendered="true">
							</div>
							<div class="col-sm-8">
								<label><?php echo $donatur->nama ?></label><br>
								<label><?php echo $donatur->alamat ?></label>
							</div>
						</div>
						<hr>
					<?php endforeach ?>


				</div>
			</div>
		</div>
	</div>

	<!-- <h2>Lokasi Kegiatan</h2> -->

	<!-- <div class="mapouter">
		<div class="gmap_canvas">
			<iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=universita%20brawijaya%20malang&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net">google map html</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style>
		</div>
	</div> -->

	<label class="container">Relawan yang bergabung</label>

	<div class="container">
		<?php foreach ($relawan as $relawan): ?>
			<img width="40px" height="40px"
				 src="<?php echo base_url(); ?>assets/foto/relawan/<?php echo $relawan->foto ?>" alt="..."
				 class="rounded-circle"> <label><?php echo $relawan->nama ?></label>
		<?php endforeach ?>
	</div>

	<br><br>

	<label class="container">Donatur yang bergabung Menjadi Relawan</label>
	<div class="container">
		<?php foreach ($kegiatan_donatur as $k): ?>
			<img width="40px" height="40px"
				 src="<?php echo base_url(); ?>assets/foto/donatur/<?php echo $k->foto ?>" alt="..."
				 class="rounded-circle"> <label><?php echo $k->nama ?></label>
		<?php endforeach ?>

	</div>


	<!-- Page Content -->
	<div class="container">

		<h5 class="font-weight-light text-center text-lg-left mt-4 mb-0">Dokumentasi Kegiatan</h5>

		<hr class="mt-2 mb-5">

		<div class="row text-center text-lg-left">
			<?php foreach ($dokumentasi as $foto): ?>
				<div class="col-lg-3 col-md-4 col-6">

					<a href="#" class="d-block mb-4 h-100">
						<img class="img-fluid img-thumbnail"
							 src="<?php echo base_url() ?>assets/foto/dokumentasi/<?php echo $foto->foto ?>" alt="">
					</a>
				</div>
			<?php endforeach ?>

		</div>

	</div>
</div>
<?php
if ($this->session->userdata("status") == "login") {
	?>
	<div class="container">
		<h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Komentar</h1>
		<form method="post" action="<?php echo base_url(); ?>index.php/dashboard/komentarKegiatan">
			<input type="hidden" name="id" value="<?php echo $key['id'] ?>">
			<input type="hidden" name="id_donatur" value="<?php echo $this->session->userdata("id_donatur"); ?>">
			<textarea class="form-control" placeholder="Isi Komentar..." name="komentar"></textarea>
			<input type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">
		</form>
	</div>
	<?php
} else {

}
?>


<div class="container">
	<label>List Komentar</label>
	<?php foreach ($komentar as $key): ?>
		<div class="row">
			<div class="col-sm-1">
				<img class="rounded-circle" alt="25x25" width="50px;" height="50pc"
					 src="<?php echo base_url(); ?>assets/foto/donatur/<?php echo $key->foto ?>"
					 data-holder-rendered="true">
			</div>
			<div class="col-sm-8">
				<label><b><?php echo $key->nama ?></b></label><br>
				<label><?php echo $key->isi_komentar ?></label>
			</div>
		</div>
	<?php endforeach ?>
</div>
<!-- Footer -->
<footer class="page-footer font-small bg-danger pt-4" style="margin-top: 10px;">

	<!-- Footer Links -->
	<div class="container text-center text-md-left">

		<!-- Footer links -->
		<div class="row text-center text-md-left mt-3 pb-3">

			<!-- Grid column -->
			<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
				<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Turun Tangan Malang</h6>
				<p style="color: white;">Gerakan Kecil Membangun Negeri</p>
			</div>
			<!-- Grid column -->

			<hr class="w-100 clearfix d-md-none">

			<!-- Grid column -->
			<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
				<h6 class="text-uppercase mb-4 font-weight-bold"></h6>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
			</div>
			<!-- Grid column -->

			<hr class="w-100 clearfix d-md-none">

			<!-- Grid column -->
			<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">

				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
				<p>
					<a href="#!"></a>
				</p>
			</div>

			<!-- Grid column -->
			<hr class="w-100 clearfix d-md-none">

			<!-- Grid column -->
			<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
				<h6 class="text-uppercase mb-4 font-weight-bold" style="color: white;">Contact</h6>
				<p style="color: white;">Facebook</p>
				<p style="color: white;">Twitter</p>
				<p style="color: white;">Instagram</p>
				<p style="color: white;">Line</p>
			</div>
			<!-- Grid column -->

		</div>
		<!-- Footer links -->

		<hr>

		<!-- Grid row -->
		<div class="row d-flex align-items-center">

			<!-- Grid column -->
			<div class="col-md-7 col-lg-8">

				<!--Copyright-->
				<p class="text-center text-md-left" style="color: white;">© 2020 Copyright:
					<a href="https://mdbootstrap.com/">
						<strong style="color: white;"> Turun Tangan Malang</strong>
					</a>
				</p>

			</div>

		</div>
		<!-- Grid row -->

	</div>
	<!-- Footer Links -->

</footer>
<!-- Footer -->

</body>
